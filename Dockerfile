# Временный образ для распаковки .jar приложения.
FROM bellsoft/liberica-openjdk-alpine-musl:16 AS builder

# Распаковываем .jar-файл.
WORKDIR                             /extracted
COPY target/*.jar                   /extracted/application.jar
RUN  java -Djarmode=layertools -jar /extracted/application.jar extract

# Начинаем ещё раз с чистого листа.
FROM bellsoft/liberica-openjdk-alpine-musl:16

# Приложение будет работать во временной зоне Europe/Moscow.
ENV TZ="Europe/Moscow"

# Установка обновлений пакетов и дополнительных инструментов.
# hadolint ignore=DL3017, DL3018
RUN apk --no-cache add curl \
                       tzdata \
  # Создаём пользователя для запуска контейнера не под root-ом.
  && mkdir -p         /x5hack-user \
  && chmod 666        /x5hack-user \
  && adduser -S -D -h /x5hack-user -u 3456 x5hack-user

# Приложение будет запускаться от имени созданного выше пользователя.
USER x5hack-user

WORKDIR                                                /x5hack-user
COPY --from=builder /extracted/dependencies/           /x5hack-user/
COPY --from=builder /extracted/snapshot-dependencies/  /x5hack-user/
COPY --from=builder /extracted/spring-boot-loader/     /x5hack-user/
COPY --from=builder /extracted/application/            /x5hack-user/

# Скопировал из starter-kit.
ENV rs.endpoint=http://localhost:9080
ENV server.port=9081

# REST API.
EXPOSE 9081
# Actuator.
EXPOSE 8081

ENTRYPOINT ["java", \
            # Настройки JVM.
            "-XX:MaxDirectMemorySize=50M", \
            "-XX:MaxMetaspaceSize=100M", \
            "-XX:ReservedCodeCacheSize=100M", \
            "-Xss512K", \
            "-Xms1G", "-Xmx1G", \
            # Look at my horse, my horse is amazing!
            "-XX:+UseShenandoahGC", \
            # Информативные NPE.
            "-XX:+ShowCodeDetailsInExceptionMessages", \
            # Разрешаем использовать Preview-фичи.
            "-XX:+UnlockExperimentalVMOptions", \
            "--enable-preview", \
            # Запуск распакованного Fat Jar.
            "org.springframework.boot.loader.JarLauncher" \
]
CMD []
