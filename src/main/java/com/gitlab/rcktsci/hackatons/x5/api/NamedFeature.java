package com.gitlab.rcktsci.hackatons.x5.api;

public interface NamedFeature {

    default String getName() {
        return getClass().getSimpleName();
    }
}
