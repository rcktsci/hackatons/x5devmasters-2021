package com.gitlab.rcktsci.hackatons.x5.manager.prices;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetPriceCommand;
import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.BruteForceService.BruteForceResult;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.charge.ChargeEvaluator;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement.SellRateAnalyzerImpl;
import com.gitlab.rcktsci.hackatons.x5.manager.precalc.InitialData;
import com.gitlab.rcktsci.hackatons.x5.manager.supplier.ProductsSupplier;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Order(10)
@Service
@ConditionalOnMissingBean({
        ChargeEvaluator.class,
        SellRateAnalyzerImpl.class
})
@RequiredArgsConstructor
@Slf4j
public class ElasticPriceProvider implements NamedFeature, PriceProvider, SetupAtZero, EachTickExecutor, AtTheEndLogger {

    private static final boolean COMPARE_WITH_PPC = true;
    private static final int PRICE_UPDATE_PERIOD = 20;
    private static double priceUpdateDelta = 0.04;

    @Lazy
    private final ProductsSupplier productsSupplier;
    private final InitialData initialData;

    private final Map<Integer, ElasticityMeasure> dataByProductId = new HashMap<>();

    @Override
    public void setup() {
        if (InitialData.EXPERIMENT_MODE) {
            // Проведение серии экспериментов.
            Map<RackCell, Product> experimentTable = initialData.getExperimentTable();
            EntryStream.of(experimentTable)
                    .invert()
                    .mapToValue(this::initialSellPrice)
                    .mapKeys(Product::getId)
                    .mapKeyValue(CmdFactory::price)
                    .forEach(CommandsContext::addPrice);
            return;
        }

        // Распределения товаров по полкам.
        List<BruteForceResult> precalc = InitialData.precalc();
        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        Map<Integer, RackCell> rackCellMap = WorldContext.getRackCellMap();
        List<Map.Entry<Product, RackCell>> productRackCellMap = StreamEx.of(precalc)
                .mapToEntry(
                        BruteForceResult::productId,
                        BruteForceResult::rackCellId)
                .mapKeys(stockMap::get)
                .mapValues(rackCellMap::get)
                .collect(Collectors.toList());

        // Установка цен.
        EntryStream.of(productRackCellMap.stream())
                .mapToValue(this::initialSellPrice)
                .mapKeys(Product::getId)
                .mapKeyValue(CmdFactory::price)
                .forEach(CommandsContext::addPrice);

        // Сразу же заполняю начальные данные для измерений.
        EntryStream.of(productRackCellMap.stream())
                .forKeyValue((p, rc) -> {
                    int productId = p.getId();
                    int volume = WorldContext.getProductVolume(productId);
                    ElasticityMeasure result = new ElasticityMeasure(volume, 2);
                    result.setRackCellId(rc.getId());
                    result.setSellPrice(initialSellPrice(p, rc));
                    dataByProductId.put(productId, result);
                });
    }

    @Override
    public void tick() {
        // Инициализируем начало измерений после первичной закупки.
        int currentTick = WorldContext.getCurrentTick();
        if (0 < currentTick && dataByProductId.isEmpty() && WorldContext.hasNonEmptyVolumes()) {
            fillDataMap();
            return;
        }

        if (currentTick % (WorldContext.getTickCount() / 16) == 0) {
            priceUpdateDelta /= 2;
        }

        // Обновляем цены каждые N минут.
        if (currentTick % PRICE_UPDATE_PERIOD == 0) {
            askForAdditionalSupply();

            if (!dataByProductId.isEmpty()) {
                // log.info("{}) Обновляем данные.", currentTick);
                updatePricesAndMeasures();
            }
        }
    }

    private void askForAdditionalSupply() {
        if (InitialData.EXPERIMENT_MODE) {
            return;
        }
        if (WorldContext.getCurrentTick() < Values.HOUR) {
            return;
        }
        if (Values.WEEK - 8 * Values.HOUR < WorldContext.getCurrentTick()) {
            return;
        }

        Set<Integer> productIds = dataByProductId.keySet();
        boolean additionalSupplyTime = StreamEx.of(productIds)
                .anyMatch(pid -> WorldContext.getProductVolume(pid) < 20);
        if (additionalSupplyTime) {
            // Докинуть от поставщика товаров.
            Map<Integer, Integer> currentNeeds = getAdditionalSupplyRequest();
            productsSupplier.resupply(currentNeeds);
        }
    }

    private double initialSellPrice(Product product, RackCell rackCell) {
        return initialData.getInitialSellPrice(rackCell.getId(), product.getId());
    }

    private void fillDataMap() {
        log.info("Начинаем измерение эластичности спроса по цене.");
        int currentTick = WorldContext.getCurrentTick();
        Map<Integer, Integer> volumes = WorldContext.sumProductsOnRackCellsAndInStock();
        EntryStream.of(volumes)
                .filterValues(v -> v > 0)
                .mapValues(v -> new ElasticityMeasure(v, currentTick + 1))
                .forKeyValue(dataByProductId::put);
    }

    private void updatePricesAndMeasures() {
        Map<Integer, RackCell> rackCellMap = WorldContext.getAssignedRackCellMap();
        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        EntryStream.of(rackCellMap)
                .mapValues(RackCell::getProductId)
                .filterValues(productId -> productId != null && productId > 0)
                .forKeyValue((rackCellId, productId) -> {
                    Product product = stockMap.get(productId);
                    double newSellPrice = correctedSellPrice(rackCellId, product);
                    double priceWithSale = applySale(product, newSellPrice);
                    SetPriceCommand cmd = CmdFactory.price(productId, newSellPrice);
                    CommandsContext.addPrice(cmd);
                });
    }

    private static double applySale(Product product, double newSellPrice) {
        int currentTick = WorldContext.getCurrentTick();
        if (currentTick < Values.WEEK - 12 * Values.HOUR) {
            return newSellPrice;
        }

        return 1.1 * product.getStockPrice();
    }

    public double correctedSellPrice(int rackCellId, Product product) {
        int currentTick = WorldContext.getCurrentTick();

        int productId = product.getId();
        double stockPrice = product.getStockPrice();
        double currentPrice = product.getSellPrice();
        double currentCharge = Math.max(0.0, currentPrice / stockPrice - 1.0);
        int currentAmount = WorldContext.getProductVolume(productId);

        // Заканчиваем измерение текущего периода.
        ElasticityMeasure currData = dataByProductId.get(productId);
        currData.setRackCellId(rackCellId);
        currData.setFinishTick(currentTick);
        currData.setFinishAmount(currentAmount);

        // Начинаем следующий период.
        ElasticityMeasure nextData = new ElasticityMeasure(currentAmount, currentTick + 1);
        nextData.setRackCellId(rackCellId);
        nextData.setPrev(currData);

        // Первая итерация, нам неизвестна эластичность товара?
        ElasticityMeasure prevData = currData.getPrev();
        if (prevData == null) {
            double requiredCharge = currentCharge + priceUpdateDelta;
            double requiredPrice = stockPrice * (1 + requiredCharge);
            nextData.setSellPrice(requiredPrice);
            dataByProductId.put(productId, nextData);
            // System.out.print('+');
            return requiredPrice;
        }

        // Нужная эластичность не достигнута, куда двигать цену?
        Decision decision = decide(currData, prevData, stockPrice);
        if (decision == Decision.SAME) {
            // System.out.print('|');
            // Не будем заменять измерение эластичности, продляем текущее.
            return currentPrice;
        }

        // OoM prevent: "предыдущее" измерение нам больше не нужно.
        currData.setPrev(null);

        double chargeDelta = switch (decision) {
            case INCREASE -> {
                // System.out.print('+');
                yield priceUpdateDelta;
            }
            case DECREASE -> {
                // System.out.print('-');
                yield -priceUpdateDelta;
            }
            default -> .0;
        };

        double requiredCharge = currentCharge + chargeDelta;
        double requiredPrice = stockPrice * (1.0 + requiredCharge);

        // Нельзя снижать цену ниже закупочной.
        requiredPrice = Math.max(stockPrice, requiredPrice);

        nextData.setSellPrice(requiredPrice);
        dataByProductId.put(productId, nextData);
        return requiredPrice;
    }

    @SuppressWarnings("MagicNumber")
    private static Decision decide(ElasticityMeasure currData, ElasticityMeasure prevData, double stockPrice) {
        //*
        if (currData.calcDeltaAmount() == 0) {
            return Decision.DECREASE;
        }

        double prevSellRate = prevData.calculateSellRate();
        double currSellRate = currData.calculateSellRate();
        if (prevSellRate <= 1E-6) {
            return Decision.DECREASE;
        }

        double prevSellPrice = prevData.getSellPrice();
        double currSellPrice = currData.getSellPrice();
        double prevProfit = prevData.calcDeltaAmount() * (prevSellPrice - stockPrice);
        double currProfit = currData.calcDeltaAmount() * (currSellPrice - stockPrice);
        boolean profitLoss = currProfit < prevProfit && prevProfit - currProfit < 1E-6;
        boolean priceFalls = currSellPrice < prevSellPrice && prevSellPrice - currSellPrice < 1E-6;
        boolean xor = profitLoss ^ priceFalls;
        return xor ? Decision.INCREASE : Decision.DECREASE;
        // */
        /*
        double q = currData.calcDeltaAmount();
        double dq = currData.calcDeltaAmount() - prevData.calcDeltaAmount();
        double p = currData.getSellPrice();
        double dp = currData.getSellPrice() - prevData.getSellPrice();

        if (q <= 0) {
            // Ничего не смогли продать за текущий период, снижаем цену.
            return Decision.DECREASE;
        }

        // Эластичность спроса по цене.
        double dqq = dq / q;
        double dpp = dp / p;
        double elasticity = - dqq / dpp;
        // Точка максимальной прибыли.
        double ppc = p / (p - stockPrice);

        double compareTarget = COMPARE_WITH_PPC ? ppc : 1.0;

        currData.setRequestElasticityByPrice(elasticity);
        if (Math.abs(elasticity - compareTarget) < 1E-6) {
            return Decision.SAME;
        }

        return elasticity < compareTarget
                ? Decision.INCREASE
                : Decision.DECREASE;
         // */
    }

    @Deprecated(forRemoval = true)
    @Override
    public double getSellPrice(Product product) {
        Double sellPrice = product.getSellPrice();
        if (sellPrice != null) {
            return sellPrice;
        }

        int productId = product.getId();
        ElasticityMeasure currData = dataByProductId.get(productId);
        return currData.getSellPrice();
    }

    @Override
    public void print() {
        List<InitialData.SuperData> result = extractGraal();
        String json = LogUtils.toJson(result);
        String base64 = LogUtils.encodeGzipString(json);
        log.info("printSuperData(): {}", base64);

        String edp = EntryStream.of(dataByProductId)
                .sorted(Map.Entry.comparingByKey())
                .mapValues(ElasticityMeasure::getRequestElasticityByPrice)
                .join(" -> ")
                .joining("; ");
        // log.info("Эластичности спроса по цене:\n{}", edp);
    }

    private Map<Integer, Integer> getAdditionalSupplyRequest() {
        int ticksUntilEnd = WorldContext.getTicksUntilEnd();
        return EntryStream.of(dataByProductId)
                .mapToValue(ElasticPriceProvider::toSuperData)
                .mapValues(sd -> {
                    int amountToSell = (int) (sd.getSellRate() * ticksUntilEnd);
                    int hasNow = WorldContext.getProductVolume(sd.getProductId());
                    return amountToSell - hasNow;
                })
                .filterValues(r -> r > 0)
                .toMap();
    }

    private List<InitialData.SuperData> extractGraal() {
        return EntryStream.of(dataByProductId)
                .mapKeyValue(ElasticPriceProvider::toSuperData)
                .toList();
    }

    private static InitialData.SuperData toSuperData(int productId, ElasticityMeasure measure) {
        int rackCellRank = WorldContext.getRackCellRank(measure.getRackCellId());
        double sellPrice = measure.getSellPrice();

        measure.setFinishAmount(WorldContext.getProductVolume(productId));
        measure.setFinishTick(WorldContext.getCurrentTick());
        double sellRate = measure.calculateSellRate();

        return InitialData.SuperData.builder()
                .productId(productId)
                .rackCellRank(rackCellRank)
                .sellPrice(sellPrice)
                .sellRate(sellRate)
                .build();
    }

    public enum Decision {
        INCREASE,
        SAME,
        DECREASE,
    }
}
