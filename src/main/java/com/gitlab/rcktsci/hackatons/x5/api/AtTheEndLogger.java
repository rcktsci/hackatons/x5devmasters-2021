package com.gitlab.rcktsci.hackatons.x5.api;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface AtTheEndLogger {

    /**
     * Текущий мир в реализациях также доступен через WorldContext.
     */
    void print();
}
