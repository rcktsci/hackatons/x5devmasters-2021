package com.gitlab.rcktsci.hackatons.x5.manager.merchandiser;

import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.BruteForceService.BruteForceResult;
import com.gitlab.rcktsci.hackatons.x5.manager.precalc.InitialData;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExperimentalRackCellFiller implements NamedFeature, SetupAtZero, EachTickExecutor {

    private final InitialData initialData;

    private List<BruteForceResult> bruteForceResults;

    @Override
    public void setup() {
        if (InitialData.EXPERIMENT_MODE) {
            log.info("Заполнение полок по таблице экспериментов.");
        } else {
            log.info("Заполнение полок по рассчитанной стратегии.");
            bruteForceResults = InitialData.precalc();
        }
    }

    @Override
    public void tick() {
        if (InitialData.EXPERIMENT_MODE) {
            // Заполнение полок по таблице экспериментов.
            Map<RackCell, Product> experimentTable = initialData.getExperimentTable();
            EntryStream.of(experimentTable)
                    .mapKeyValue(CmdFactory::toRackCell)
                    .forEach(CommandsContext::addOnRack);
            return;
        }

        // Заполнение полок по рассчитанной стратегии.
        Map<Integer, RackCell> rackCellMap = WorldContext.getRackCellMap();
        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        StreamEx.of(bruteForceResults)
                .mapToEntry(
                        BruteForceResult::rackCellId,
                        BruteForceResult::productId)
                .mapKeys(rackCellMap::get)
                .mapValues(stockMap::get)
                .mapKeyValue(CmdFactory::toRackCell)
                .forEach(CommandsContext::addOnRack);
    }
}
