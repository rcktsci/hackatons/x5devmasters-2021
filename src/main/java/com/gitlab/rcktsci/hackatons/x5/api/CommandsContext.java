package com.gitlab.rcktsci.hackatons.x5.api;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.BuyStockCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentTickRequest;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.FireEmployeeCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.HireEmployeeCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.PutOffRackCellCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.PutOnRackCellCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetOffCheckoutLineCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetOnCheckoutLineCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetPriceCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Slf4j
public class CommandsContext {

    private static final ThreadLocal<CommandsContext> context = new ThreadLocal<>();

    private final List<HireEmployeeCommand> hire = new ArrayList<>();
    private final List<FireEmployeeCommand> fire = new ArrayList<>();
    private final List<SetOnCheckoutLineCommand> toCheckout = new ArrayList<>();
    private final List<SetOffCheckoutLineCommand> fromCheckout = new ArrayList<>();
    private final List<BuyStockCommand> buy = new ArrayList<>();
    private final List<SetPriceCommand> price = new ArrayList<>();
    private final List<PutOnRackCellCommand> onRack = new ArrayList<>();
    private final List<PutOffRackCellCommand> offRack = new ArrayList<>();

    public CurrentTickRequest toRequest() {
        CurrentTickRequest result = new CurrentTickRequest();
        result.hireEmployeeCommands(hire);
        result.fireEmployeeCommands(fire);
        result.setOnCheckoutLineCommands(toCheckout);
        result.setOffCheckoutLineCommands(fromCheckout);
        result.buyStockCommands(buy);
        result.setPriceCommands(price);
        result.putOnRackCellCommands(onRack);
        result.putOffRackCellCommands(offRack);
        return result;
    }

    public static void addHire(Collection<HireEmployeeCommand> cmds) {
        get().hire.addAll(cmds);
    }

    public static void addHire(HireEmployeeCommand cmd) {
        get().hire.add(cmd);
    }

    public static void addFire(Collection<FireEmployeeCommand> cmds) {
        get().fire.addAll(cmds);
    }

    public static void addFire(FireEmployeeCommand cmd) {
        get().fire.add(cmd);
    }

    public static void addToCheckout(Collection<SetOnCheckoutLineCommand> cmds) {
        get().toCheckout.addAll(cmds);
    }

    public static void addToCheckout(SetOnCheckoutLineCommand cmd) {
        get().toCheckout.add(cmd);
    }

    public static void addFromCheckout(Collection<SetOffCheckoutLineCommand> cmds) {
        get().fromCheckout.addAll(cmds);
    }

    public static void addFromCheckout(SetOffCheckoutLineCommand cmd) {
        get().fromCheckout.add(cmd);
    }

    public static void addBuy(Collection<BuyStockCommand> cmds) {
        get().buy.addAll(cmds);
    }

    public static void addBuy(BuyStockCommand cmd) {
        get().buy.add(cmd);
    }

    public static void addPrice(Collection<SetPriceCommand> cmds) {
        get().price.addAll(cmds);
    }

    public static void addPrice(SetPriceCommand cmd) {
        get().price.add(cmd);
    }

    public static void addOnRack(Collection<PutOnRackCellCommand> cmds) {
        get().onRack.addAll(cmds);
    }

    public static void addOnRack(PutOnRackCellCommand cmd) {
        get().onRack.add(cmd);
    }

    public static void addOffRack(Collection<PutOffRackCellCommand> cmds) {
        get().offRack.addAll(cmds);
    }

    public static void addOffRack(PutOffRackCellCommand cmd) {
        get().offRack.add(cmd);
    }

    @NonNull
    public static CommandsContext get() {
        return Optional.ofNullable(context.get())
                .orElseThrow();
    }

    public static void reset() {
        context.set(new CommandsContext());
    }
}
