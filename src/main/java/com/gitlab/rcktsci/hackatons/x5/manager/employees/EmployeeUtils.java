package com.gitlab.rcktsci.hackatons.x5.manager.employees;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.Employee;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.HireEmployeeCommand.ExperienceEnum;
import lombok.experimental.UtilityClass;

import static org.apache.commons.lang3.StringUtils.left;

@UtilityClass
public class EmployeeUtils {

    public final int JUNIOR_MAX_XP = 30;
    public final int MIDDLE_MAX_XP = 70;

    public ExperienceEnum experienceToType(Employee employee) {
        int experience = employee.getExperience();

        if (experience <= JUNIOR_MAX_XP) {
            return ExperienceEnum.JUNIOR;
        }

        if (experience <= MIDDLE_MAX_XP) {
            return ExperienceEnum.MIDDLE;
        }

        return ExperienceEnum.SENIOR;
    }

    public String toPrettyString(Employee employee) {
        return "# %d, %s %s. (xp %d)".formatted(
                employee.getId(),
                employee.getLastName(),
                left(employee.getFirstName(), 1),
                employee.getExperience());
    }
}
