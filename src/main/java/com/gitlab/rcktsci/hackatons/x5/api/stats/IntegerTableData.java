package com.gitlab.rcktsci.hackatons.x5.api.stats;

import java.io.Serializable;

public record IntegerTableData(String name, int[][] csv) implements Serializable {

}
