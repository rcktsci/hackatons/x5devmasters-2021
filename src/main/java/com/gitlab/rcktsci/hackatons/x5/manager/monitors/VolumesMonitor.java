package com.gitlab.rcktsci.hackatons.x5.manager.monitors;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.api.stats.AmountPerTickStatistic;
import com.gitlab.rcktsci.hackatons.x5.api.stats.ListPerTickStatistic;
import com.gitlab.rcktsci.hackatons.x5.api.stats.ReduceMode;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentWorldResponse;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Customer;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class VolumesMonitor implements EachTickExecutor, AtTheEndLogger {

    private final AmountPerTickStatistic customersInHall = new AmountPerTickStatistic("in-hall");
    private final AmountPerTickStatistic customersAtCheckout = new AmountPerTickStatistic("at-checkout");
    private final ListPerTickStatistic totalVolumes = new ListPerTickStatistic("product-volumes", ReduceMode.AVG);

    @Override
    public void tick() {
        CurrentWorldResponse world = WorldContext.get();

        // Глянем статистику.
        getStatistics(world);
    }

    private void getStatistics(CurrentWorldResponse world) {
        int currentTick = world.getCurrentTick();

        var customers = StreamEx.of(world.getCustomers()).groupingBy(Customer::getMode);
        var whoInHall = customers.getOrDefault(Customer.ModeEnum.IN_HALL, List.of());
        var whoAtCheckout = customers.getOrDefault(Customer.ModeEnum.AT_CHECKOUT, List.of());
        customersInHall.add(currentTick, whoInHall.size());
        customersAtCheckout.add(currentTick, whoAtCheckout.size());

        totalVolumes.put(currentTick, sumTotalProducts(world));
    }

    private static List<Integer> sumTotalProducts(CurrentWorldResponse world) {
        List<Integer> values = new ArrayList<>();

        Map<Integer, Product> stock = WorldContext.getStockMap();
        EntryStream.of(stock)
                .sorted(Map.Entry.comparingByKey())
                .values()
                .map(Product::getInStock)
                .forEach(values::add);

        Map<Integer, RackCell> rackCells = WorldContext.getRackCellMap();
        EntryStream.of(rackCells)
                .values()
                .mapToEntry(RackCell::getProductId, RackCell::getProductQuantity)
                .nonNullKeys()
                .mapKeys(i -> i - 1)
                .nonNullValues()
                .mapToValue((idx, q) -> values.get(idx) + q)
                .forKeyValue(values::set);

        return values;
    }

    @Override
    public void print() {
        totalVolumes.print();
    }
}
