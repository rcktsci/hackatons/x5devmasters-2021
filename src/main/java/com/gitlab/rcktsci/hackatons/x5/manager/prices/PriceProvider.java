package com.gitlab.rcktsci.hackatons.x5.manager.prices;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface PriceProvider {

    double getSellPrice(Product product);

    default double getSellPrice(Product product, int rackCellRank) {
        return getSellPrice(product);
    }
}
