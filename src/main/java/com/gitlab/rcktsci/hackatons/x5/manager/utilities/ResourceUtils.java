package com.gitlab.rcktsci.hackatons.x5.manager.utilities;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentWorldResponse;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@UtilityClass
public class ResourceUtils {

    public CurrentWorldResponse worldFromJson() {
        Gson gson = new Gson();
        return Optional.of("/world.json")
                .map(ResourceUtils.class::getResource)
                .map(ResourceUtils::toURI)
                .map(Paths::get)
                .map(ResourceUtils::readFile)
                .map(json -> gson.fromJson(json, CurrentWorldResponse.class))
                .orElseThrow();
    }

    @SneakyThrows
    private URI toURI(URL url) {
        return url.toURI();
    }

    @SneakyThrows
    private String readFile(Path path) {
        return Files.readString(path);
    }
}
