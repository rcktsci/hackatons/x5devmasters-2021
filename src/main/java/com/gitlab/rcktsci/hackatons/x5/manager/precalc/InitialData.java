package com.gitlab.rcktsci.hackatons.x5.manager.precalc;

import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.BruteForceService;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.BruteForceService.BruteForceResult;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement.SellRateResult;
import com.gitlab.rcktsci.hackatons.x5.manager.prices.PriceProvider;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.ResourceUtils;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.gitlab.rcktsci.hackatons.x5.manager.prices.StaticPriceWithFinalSale.DESIRED_PROFIT;

@Order(1)
@Component
@RequiredArgsConstructor
@Slf4j
public class InitialData implements SetupAtZero {

    public static final boolean EXPERIMENT_MODE = false;

    /**
     * Нужно поставить 45 / 3 = 15 экспериментов :(
     * <br/>
     * Точнее, с <b>0-го по 14-й</b>.
     */
    public static final int EXPERIMENT_SERIES_ID = 14;

    private static final Map<Integer, String> SUPER_0_DATA = EntryStream.<Integer, String>empty()
            // EXPERIMENT_SERIES_ID => Base64(Gzip(JSON(SuperData)))
            // @formatter:off
            //*
            .append(0, "H4sIAAAAAAAC/4XSO07EMBAG4LukXo087/FeBW0DNHRoERXauxOEPXYsUNKkSPz5n8fT1/b+9rpd8bLdX+6/78/n7WoCLDWfkMv2sX8uwF4ZC4sEuYbR49IEWgQJqFbZu2KDyMPFopolwQuBCBXDRw5KYo5QrCYhjaBGUAHVUUe1LkwBVNFJktCFYAY2JD292/44SIHU82MSP/fh6CQn4Y3gHl9BB7DH904cWqg65hALgRyAUxf9dAx1EdzA9r8yRQ5SlhApYGmE9HUykLmONkiEckzhg8CFUAG0UUYk8d84kBbBHYinXUjgOI3RSuwLqb2XzmBVcqnjnJCF4ArhUwg9HSjqQtgegqZmYif0GKM+bt+5LBay3wMAAA==") // 13.09.2021 00:02
            .append(1, "H4sIAAAAAAAC/4XSO27DMAwG4LtoNghRFF+5SpGl7dKtSNGp6N1rI3qFRWAtGix9/knq5Sd9frynS93S7e2WLrjv36/77kDqc8mWvvbPGahWK8omJYu5yO/WBA4CZfCs9b+w3GZGLXUQEojiwGpYBtKJxwzMQ9AmlC4IZFpCcBeO/+JeIWE+KqIhWBD2Q+BLBqXRifvVHsQH4YEwAraZwnSEmAHuyiAwN4OaIUchdSCG3Qi9xElgINhAXGwQfDpSLIGwAuujmJU8HQhSI2rvpzgYzhhtphgnsvSiBoIMfF103gsOhCAUwhlCzlP0x8m9nQhKU3A9JzQQWAWsLv3kbjwKuozEolEIspRFeWpc/wDmJyEc7QMAAA==") // 13.09.2021 00:02
            .append(2, "H4sIAAAAAAAC/43SvU7EMAwA4HfJXEXxX+z0VRAL3HIbOsSEeHcCbeqcERQvHRp9/n14Ty/XS1p1SbfnW1qhf9+e0oqUrcxhS3rt/0tGBtJGUIjZUOvHshMWiP4sa6nIbQsXVKxiqdaqCCjyQbRAqGYQcOEgttwH40VA2QkcBOXGUxUwiK/EUyfkBARCOBPpAJrxIO5qEEEnMBCGGal5yDlBO0FjnNonJ1MZOAy5b6W5wcEgy/2Zxz86kUDU2juZllrPtwp1N3gMFDJWN3waQQAnNBDA0jMeQlM7Nywa0A9UphunzYB4G9M4xoXKMErL1bwQxVMDSzC+T/TncfxFQCCQ+g7Vy+BficdP6m0e2PIDAAA=") // 13.09.2021 00:03
            .append(3, "H4sIAAAAAAAC/43SS07EMAwG4Lt0PbLitzNXQbMBNuzQIFaIuxPRvCYItdm0apUvvx0/fW3vb6/bFdNlu7/cy0t5fj5vV1cQZSRN+yrfP8rvBMkimyo6CbJnxu9LM3AxVACRJNcV3oy6M7FIkGvYMGjNYYB5WnbC4GpQNdARQthbkmhG30u/VU2ELIQQpDlGb8ecoCh5ELoQhuA+QuQ4U4pVhFtLETCGkU16kEeChuGLgVpucTL8mIiVIAYbN3uqH7ka0owUYHMM3o1SYZ+t3eoGpcVwAplSBDXiEfDRUcKFIIoyHGNMtVUyz3hhdBBtRrURnEuM1BtifmzwaoQA+RhS9eNuyJ8cAeX0Ph+m/xq3H3bTw5j5AwAA") // RESTART 13.09.2021 00:15
            .append(4, "H4sIAAAAAAAC/4XSTU7DMBAF4LtkHY08vx5zFcQGumGHilgh7o5FbI9j1NabRGrz+fl5nr+3j/fL9oS8b9e3a32pz6/X+nQFJvMs5Vj79ll/ToCZBDkXxsQizj97J2QhuICVaVknTgBlC0IXQgu4IWlqiztxFtQnw5pB3UCo/xopfJykfUnJvJgqBZEXApXAaDLGUcb+hzLF8NUgA4w6i4822v6j2DBKM7gbyFBSJLHHZ6G0GJlhKjThqNTOQTwIXAgSAVfGniPaON/JlIIaIZ1ggjKNlx4E3p4N4pXIDsmiDJZuLCM6xZB/MapBcS0ybmUq848Lo8+oNsMJlDVyyOOj2EKgJKhbrW3cS5FXQmv9No3o7TZefgFe1es99QMAAA==") // 13.09.2021 00:04
            .append(5, "H4sIAAAAAAAC/43STU7EMAwF4Lt0XVmOnfhnroJmA2zYoUGs0Nyd0EnqNEiUblqp7afn5zx9Le9vr8slybrcXm71od4/n5dLUchomr1deV0+6msEFHMpJSnlxOqc7ms3dDJSSVA/818GJ+ScjbSYEIoHYTNBBQbBvRPD71ueILwR1AlUIIlRbCce+SPKbhBOhhJIdOEm/zDSZJAgCKeY5rxRomZwN8hBJNooe5Chig0Kg2fD6zDZ9iDnW6E8E8zAjtIJ4b2P41YojNKM3AxDMAnC04NIQMdOh0ZlIhIL8ED0tfxl6GxkrYMPq7VuxD4eShj9kJZm1ELHI6q6L6UckDik5BPxM4pyXUrB7aLTFIxzCgPLccyHFEfD79dvN92/xPYDAAA=") // 13.09.2021 00:05
            .append(6, "H4sIAAAAAAAC/43TTU7EMAwF4LtkXVlxHP/NVRAbYMMODWKFuDstJHEmUmG66aLS1+eX+OEzvb2+pAuVLV2fr+nC+/vjKV2KKaCW6u3RLb3vnxFKRVInzFSrFZWvrRO0EFwgs5gOoxOHOyODQG8ENgKzgWAQgr9GPo9R8mIogWMM4nUQyiYli7kwH5nCwMUohWEehe4weqOlG1QBiUYU6cbPABMTBC0EZQexSj0Hjxw3ZRxWIHXNIQgcxtTHaaXcCGrEfjcM41hsEMvJUhCyEEgOKlGp/d+GrgQzqE6XoxPnXVgjaiN8/5/E1TDuBN9O4kH4QiAx5Bx13mFQXmMwoEeMPskfm0K47utuGE/7Wk8X9vEb00jbV/UDAAA=") // 13.09.2021 00:05
            .append(7, "H4sIAAAAAAAC/4XTvU7FMAwF4HfJfGXFP3HivgpiARY2dBET4t1JRROnRlft0qHy1+OT9uk7fby/pY3plu6v97RJv3+9pI0aAksVOy7uDz77cwQS5GqMmUUaVf25DYODURCMqFwPyjFYxqCC2nx1f7leGyUYFQGz57c/IUMpWBeluKBRYGgLYDPECegxJkGjRRwtcoY+OBkZKeYGpSllNSc4EJwbaOUZhAcRUrAT8i8FQV4WKTpjLBmaqddJo046jKZgVTzG9SIaBGSD5oDVSSwHuitO1EiIgJgbtQ3jvEfxU6V2GHwYZvvWXgYO4jS/l+uGBQOlF4pZKbbx2OAcjUz9I18umrtgaMQRjH+pdUQZRxCVh9/o8y9T+HH76wMAAA==") // 13.09.2021 00:06
            .append(8, "H4sIAAAAAAAC/4XTu07FMAwG4HfpXFmx4yuvgs4CLGzoICbEu5NCbo1A7ZJKVb/8dpzHz+3t9WV7yLRv9+d7eSnrx1NZU4B79Mf27b18TkBJPVQEjRizRf7am5EXQwQ41I2rwd0wcZ2hYXA1uBl67EiN8NyMEQBTZo5ByEKog1MPEUG/BMJZcBqELoQRsKTx5GvCKiGVQAZzGimiEWfAdBC+Eg7sUyF+TcRCECp4mo7VLiuh1k9szXCQiNyDeDuSY+/pYCdCF6JsBC4jRmgz5joOaxi2GpqBBP8Yjfn/nywDaS2likSCFFNHew5eihlELARSuSmpzGgdELTrEU2rkRBYpc+5S+/HuZjJwOW2USgoYh8Q+reU2ze4ojGo9gMAAA==") // 13.09.2021 00:06
            .append(9, "H4sIAAAAAAAC/43TvU7EMAwH8HfpfLLi2I7texXEAixs6BATuncnR/NVD5Qskdrm17+d5Ol7+3h/266UL9vt9bZdH/PXS52tgJiPUZ9/1tcJGEmdMBGzZRUr90s3KBiCUNIyaBj7ypyKeRHJk+BGUCccsCj3FEb/iCHBKAqWZyXO3RDUvDg+iRIIZdCZwq0L4+97KUsIbQI3ARko+Tp2A+EYwpZmWDQMkjHNIHxueDAyEtAaY2zssiG/3DA4NUOaQWqAhqYy1h5bKXMtxrWiwHm2Uk9L4BwIJIHaBYnn6g8j91ZiMxzr3i2d7IUcOvCQJuGBQK6fa5m1jFNxDKHLqUgxRq3EcRJ4fkMwXDLKCRzrZnA43fGC5PvzDwewmvnuAwAA") // 13.09.2021 00:07
            .append(10, "H4sIAAAAAAAC/43TTU4DMQwF4LvMehTFv7F7FdQNsGGHilgh7k6EkjiTAkM3lTqaT8/P7sPH9vryvF0I9+32dNsuUL/fH+sPWZKqWmH//mDZt7f6PCcoyEDFCTIxG33u3aDFEEjABNgIl07E21jEFIPgRmAQmKmHcKdOyDGGByELoZZAsvYUdi7oIhgmHwmq4OdVlEZQI0AS62yMOg9FZJ1i2GqUGmPqgsco8bq5ikx9+mJgDeIYOZSHActWBsK5Idxvo2CNHXv9jwGrwZ50ug1rxwFrqTEM42IAczIZm3W101K536h0A+p5UBTSh6l3d5xmmoXvDEpk98v9axZZjVwSYizX8dQg+OE/ayX6gF/ruH4BJeuJ1vgDAAA=") // 13.09.2021 00:08
            .append(11, "H4sIAAAAAAAC/4XSsU7EMAwG4HfJXFmxY8dOXwWxAAsbOsSEeHdCSeI0h+gtHc7+9Dv2w2d4e30JO27h9nwLu9Tvx1PYc4IUi/90C+/1bwRUYkxaEsbEbPS1NYEWQSJIIh5CuhQSNwI7IYC1chgkv0aEo1HFMsVsJWc3lhRIIDwRyp2Qc47ihCwx1H6KfZJBRO/J9z2I9EfPlFqOCG5oM6hnZzCedpBzR87Ti7hhq2FgKHkk0WH052/ONH9ZDKpBSD2HXcbg2IjUiKQEtcZHGVs4r2EicCVEged7SmMSv6UjjRu0GMi12nyVpmMtp5NUPyjuF8XdQIHpMYrh5WEz3xkIcXqOvpX/DFmNKBDFH8RkGMtiH78BRy93YOUDAAA=") // 13.09.2021 00:08
            .append(12, "H4sIAAAAAAAC/4XTPW7DMAwF4LtoNgjxTxR9lSJLmyVbkaJT0btXqS1ZURLIiwfDnx8f5bef8Hk5hxWXcP24hlXK/fs9rInBmMTrtYSv8jSCKhoJsjljZNHfZQdoAMRBox1A0o1A6F6XTJYawQOBBL1QjCkhO6GVcLDYCdmmhA4EEyimZ1XkRDHlMtnWyTGH7QTWFAqYukHUqtG+v2HejDSmQMDo3OqweYo8pigb4X4jVI074NbKgfiAUAmStFsJN+TlTuJuUJ3Fb/XnFiXPCXwgFPLTRu/6/J/qUGhQkBU8d5W0YQakS1IPKVcDBWJXiNn0VxF5MAz6GOrT8yE6GrEcshK2Mvn1CTn9AavMS5j1AwAA") // 13.09.2021 00:09
            .append(13, "H4sIAAAAAAAC/43Tu07FMAwG4HfJXFlx7PjSV0EswMKGDmJCvDtBJGkwOmq9dKjy9bfrPHymt9eXtOOWbs+3tFN7fjylXQiyFfZepnVL7+19BkZSJ8zEbEWrydfWjRIMdmDKMhUcRDtUsphLraiFcQoUBCQg05nC1Qbx5/xPoGlwN3gYDkX8KPklEJYWmuYTqAEgBPIjg9sZIBHIgLzMwY4II32nJqGdqJ0oDLTMwYVPCQsEEoH7opwKnDuBoxF3yEg4OqHxN+jeKDyE0AqKRwS7EAJjCKngPPuwKytRgoGsIMs8jQcScizG2M0yjNL6rsta+IUgHBFk8LXmHQmXbBlI/We0T+pyV+8bj9/Z1/rb7wMAAA==") // 13.09.2021 00:09
            .append(14, "H4sIAAAAAAAC/4XSu04DMRAF0H9xvbI8nqf3V1AaSEOHgqgQ/44j1uNHFNZNimiP7szcl+/w8X4NO2zh9nYLe66/X69hF4lmVNpT28Jn/TvFTIBaEBISWVb52Q4hLwKVqFZQG0JN+PuQTXISK9IFXATIERFyT+HEnKFSbtBh4GHkFFlLf4IeYxayE7wQeI+hwy48xTQHswuyCimWJH0QsfNB9DCoDUJxXEVLAXGcIUlxwBYA6hw2Cg7ofFMXyiIo1Yvqwzn+ESAdBLda6Z3g1B6cE7AQzJGQ+kFOBWq9grYJoEhDKUz8HPNBhxR5SaG13UOtiteKeQrSS0H0EEPqMvpFegx42k3iFUkQFdnLZeD9Xotx+QX2Ug/B7AMAAA==") // 13.09.2021 00:10
            // */
            // @formatter:on
            .toMap();

    private static final Map<Integer, String> SUPER_RUNTIME_DATA = EntryStream.<Integer, String>empty()
            // @formatter:off
            // .append(0, "H4sIAAAAAAAC/21Su04EMQz8l9QrK37H+yvoGqChQ4eoEP/O7imPIxs3KeIZz4z98pM+P97TzrSl+9s97Xq8369pp6JAHuwSj/ItfR3fGfLv1jBRMVQx6Ab52ie59nHt4zBwyUaVuywwOOlhVYgm5tQjCxBXEDZBGKeJXtZdKDoJ8mEQM4tE58DGIY3DAiKPwYWvg9HnJCivksAyC2QDK8MWWQdZCdMnmZ2EcIoTi6+GEU1OiAnUhpNz5xfQbJ/cIQi7xkXsJJMgkgKk43ZsBbI5smPBxB0T3qN4bKiQazF6xDIuME95hoA87yoax38C1UExHxoFAoqwaz4Lh/TbH2UMQ1otAwAA")
            // @formatter:on
            .toMap();

    private static final List<SuperData> SUPER_MEASURE_DATA = EntryStream.<Integer, String>empty()
            .prepend(SUPER_RUNTIME_DATA)
            .append(SUPER_0_DATA)
            .values()
            .filter(StringUtils::isNotBlank)
            .map(LogUtils::decodeGzipString)
            .map(json -> LogUtils.fromJson(json, SuperData[].class))
            .flatMap(Arrays::stream)
            .distinct(e -> e.getRackCellRank() * 100 + e.getProductId())
            .sorted(Comparator
                    .comparing(SuperData::getProductId)
                    .thenComparing(SuperData::getRackCellRank))
            .toList();

    @Override
    public void setup() {
        log.info("DESC: Ещё другой брутфорс ^_^");
        if (EXPERIMENT_MODE) {
            log.info("EXPERIMENT_SERIES_ID = {}", EXPERIMENT_SERIES_ID);
        } else {
            log.info("Not in EXPERIMENT_MODE.");
        }
    }

    @SuppressWarnings({"PointlessArithmeticExpression", "MagicNumber"})
    public Map<RackCell, Product> getExperimentTable() {
        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        Map<Integer, RackCell> rackCellMap = WorldContext.getRackCellMap();
        return IntStreamEx.range(15)
                .boxed()
                .mapToEntry(
                        // ID полки.
                        idx -> 1 + idx,
                        // ID товара для расположения на этой полке.
                        idx -> 1 + (idx + 3 * InitialData.EXPERIMENT_SERIES_ID) % 45)
                .mapKeys(rackCellMap::get)
                .mapValues(stockMap::get)
                .toMap();
    }

    public double getInitialSellPrice(int rackCellId, int productId) {
        Map<Integer, RackCell> rackCellMap = WorldContext.getRackCellMap();
        RackCell rackCell = rackCellMap.get(rackCellId);
        int rackCellRank = rackCell.getVisibility();

        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        Product product = stockMap.get(productId);
        double stockPrice = product.getStockPrice();

        double data = StreamEx.of(SUPER_MEASURE_DATA)
                .filterBy(SuperData::getProductId, productId)
                .filterBy(SuperData::getRackCellRank, rackCellRank)
                .maxBy(sd -> sd.calcWeeklyProfit(stockPrice))
                .map(SuperData::getSellPrice)
                .orElse(0.0);

        if (0.0 < data) {
            return data;
        }

        return stockPrice * (1.0 + DESIRED_PROFIT);
    }

    @Value
    @Builder
    public static class SuperData {

        @SerializedName("pid")
        int productId;

        @SerializedName("rcr")
        int rackCellRank;

        @SerializedName("rub")
        double sellPrice;

        @SerializedName("sr")
        double sellRate;

        public double calcWeeklyProfit(double stockPrice) {
            int volume = (int) (sellRate * Values.WEEK);
            return volume * (sellPrice - stockPrice);
        }
    }

    public static void main(String[] args) {
        WorldContext.executeWithinWorldContext(
                ResourceUtils.worldFromJson(),
                () -> {
                    List<BruteForceResult> bruteForceResults = precalc();
                    StreamEx.of(bruteForceResults)
                            .sortedBy(BruteForceService.BruteForceResult::rackCellId)
                            .forEach(System.out::println);
                });
    }

    public static List<BruteForceResult> precalc() {
        List<SellRateResult> sellRateResults = StreamEx.of(SUPER_MEASURE_DATA)
                .map(sd -> SellRateResult.builder()
                        .productId(sd.getProductId())
                        .rackCellRank(sd.getRackCellRank())
                        .sellRate(sd.getSellRate())
                        .build())
                .toList();

        PriceProvider priceProvider = new InitialDataPriceProvider(SUPER_MEASURE_DATA);
        List<BruteForceResult> bruteForceResults = BruteForceService.bruteForce(
                sellRateResults,
                false,
                priceProvider);

        double totalExpectedIncome = StreamEx.of(bruteForceResults)
                .map(BruteForceResult::expectedIncome)
                .mapToDouble(AtomicReference::get)
                .sum();
        log.info("Total expected income: {}", totalExpectedIncome);

        return bruteForceResults;
    }
}
