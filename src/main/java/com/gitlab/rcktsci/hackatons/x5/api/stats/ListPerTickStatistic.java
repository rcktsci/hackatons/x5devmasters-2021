package com.gitlab.rcktsci.hackatons.x5.api.stats;

import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.springframework.lang.NonNull;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Slf4j
public class ListPerTickStatistic {

    private static final int LOD = 60;

    private final Map<Integer, List<Integer>> listsPerTick = new HashMap<>();

    private final String name;
    private final ReduceMode reduceMode;
    private final int lod;

    public ListPerTickStatistic(String name, ReduceMode reduceMode) {
        this.name = name;
        this.reduceMode = reduceMode;
        this.lod = LOD;
    }

    public void put(int tick, List<Integer> values) {
        listsPerTick.put(tick, values);
    }

    public void print() {
        String csv = toOutputCsv();
        String bzip2Csv = LogUtils.encodeBZip2(name, csv);
        String gzipCsv = LogUtils.encodeGzip(name, csv);

        IntegerTableData intTable = toIntegerTable();
        String encodedIntTable = LogUtils.encodeIntegerTable(intTable);

        EntryStream.of(
                        "BZip2 .csv", bzip2Csv,
                        "GZip .csv", gzipCsv,
                        "IntTable", encodedIntTable)
                .reverseSorted(Comparator.comparing(e -> e.getKey().length()))
                .limit(1)
                .forKeyValue((type, data) -> log.info("{} for {}: {}", type, name, data));
    }

    private String toOutputCsv() {
        return getReducedDataStream()
                .mapValues(l -> StreamEx.of(l)
                        .joining(";"))
                .mapKeyValue("%d;%s"::formatted)
                .joining("\n");
    }

    private IntegerTableData toIntegerTable() {
        int[][] arrays = getReducedDataStream()
                .mapKeyValue((t, l) -> StreamEx.of(l)
                        .mapToInt(i -> i)
                        .prepend(t)
                        .toArray())
                .toArray(int[].class);
        return new IntegerTableData(name, arrays);
    }

    private EntryStream<Integer, List<Integer>> getReducedDataStream() {
        return EntryStream.of(listsPerTick)
                .sorted(Map.Entry.comparingByKey())
                .mapKeys(k -> k - k % lod + lod / 2)
                .collapseKeys()
                .mapValues(this::reduce);
    }

    private List<Integer> reduce(@NonNull List<List<Integer>> l) {
        return switch (reduceMode) {
            case LAST -> l.get(l.size() - 1);
            case AVG -> reduceList(l);
        };
    }

    private static List<Integer> reduceList(@NonNull List<List<Integer>> l) {
        return switch (l.size()) {
            case 0 -> List.of();
            case 1 -> l.get(0);
            default -> average(l);
        };
    }

    private static List<Integer> average(@NonNull List<List<Integer>> l) {
        int columns = l.get(0).size();
        return IntStreamEx.range(columns)
                .map(cid -> l.stream().map(ll -> ll.get(cid)).findFirst().orElse(0))
                .boxed()
                .toList();
    }
}
