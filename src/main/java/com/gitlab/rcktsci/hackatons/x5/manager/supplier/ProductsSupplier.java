package com.gitlab.rcktsci.hackatons.x5.manager.supplier;

import java.util.Map;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface ProductsSupplier {

    void resupply(Map<Integer, Integer> currentNeeds);
}
