package com.gitlab.rcktsci.hackatons.x5.manager.utilities;

import com.gitlab.rcktsci.hackatons.x5.api.stats.IntegerTableData;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipParameters;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.zip.Deflater;

@UtilityClass
@Slf4j
public class LogUtils {

    public final Gson GSON = new Gson();

    public String toJson(Object payload) {
        return GSON.toJson(payload);
    }

    public <T> T fromJson(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }

    @SneakyThrows
    public String encodeIntegerTable(IntegerTableData integerTableData) {
        try (var baos = new ByteArrayOutputStream();
             var cos = new BZip2CompressorOutputStream(baos, 9);
             var oos = new ObjectOutputStream(cos)) {
            oos.writeObject(integerTableData);
            oos.close();
            cos.finish();
            byte[] compressed = baos.toByteArray();
            return Base64.getEncoder().encodeToString(compressed);
        }
    }

    @SneakyThrows
    public IntegerTableData decodeIntegerTable(String encoded) {
        byte[] compressed = Base64.getDecoder().decode(encoded);

        try (var bais = new ByteArrayInputStream(compressed);
             var cis = new BZip2CompressorInputStream(bais);
             var ois = new ObjectInputStream(cis)) {
            Object object = ois.readObject();
            if (object instanceof IntegerTableData integerTableData) {
                return integerTableData;
            }

            throw new IllegalArgumentException("Not a LogData.");
        }
    }

    @SneakyThrows
    public String encodeBZip2(String name, String csv) {
        String data = name + '\n' + csv;
        byte[] bytes = data.getBytes(StandardCharsets.UTF_8);

        try (var baos = new ByteArrayOutputStream(bytes.length);
             var cos = new BZip2CompressorOutputStream(baos, 9)) {
            cos.write(bytes);
            cos.finish();
            byte[] compressed = baos.toByteArray();
            return Base64.getEncoder().encodeToString(compressed);
        }
    }

    @SneakyThrows
    public LogDecodeResult decodeBZip2Csv(String encoded) {
        byte[] compressed = Base64.getDecoder().decode(encoded);
        try (var bais = new ByteArrayInputStream(compressed);
             var cis = new BZip2CompressorInputStream(bais)) {
            return fromCompressedStream(new String(cis.readAllBytes(), StandardCharsets.UTF_8));
        }
    }

    public String encodeGzip(String name, String csv) {
        String data = name + '\n' + csv;
        return encodeGzipString(data);
    }

    @SneakyThrows
    public String encodeGzipString(String data) {
        byte[] bytes = data.getBytes(StandardCharsets.UTF_8);

        GzipParameters parameters = new GzipParameters();
        parameters.setCompressionLevel(Deflater.BEST_COMPRESSION);

        try (var baos = new ByteArrayOutputStream(bytes.length);
             var cos = new GzipCompressorOutputStream(baos, parameters)) {
            cos.write(bytes);
            cos.finish();
            byte[] compressed = baos.toByteArray();
            return Base64.getEncoder().encodeToString(compressed);
        }
    }

    public LogDecodeResult decodeGzipCsv(String encoded) {
        String data = decodeGzipString(encoded);
        return fromCompressedStream(data);
    }

    @SneakyThrows
    public String decodeGzipString(String encoded) {
        byte[] compressed = Base64.getDecoder().decode(encoded);
        try (var bais = new ByteArrayInputStream(compressed);
             var cis = new GzipCompressorInputStream(bais)) {
            byte[] payload = cis.readAllBytes();
            return new String(payload, StandardCharsets.UTF_8);
        }
    }

    private static LogDecodeResult fromCompressedStream(String data) {
        List<String> lines = StreamEx.of(data.lines()).toList();
        String name = lines.get(0);
        String payload = StreamEx.of(lines).skip(1).joining("\n");

        return new LogDecodeResult(name, payload);
    }

    @SneakyThrows
    public static String readClipboardBase64() {
        try {
            Clipboard clipboard = Toolkit.getDefaultToolkit()
                    .getSystemClipboard();
            Object data = clipboard.getData(DataFlavor.stringFlavor);

            if (data instanceof String string) {
                byte[] decoded = Base64.getDecoder().decode(string);
                return Base64.getEncoder().encodeToString(decoded);
            }
        } catch (Exception ignored) {
        }

        throw new IllegalStateException("Скопируй в буфер Base64!");
    }

    public static record LogDecodeResult(String name, String payload) {

        public static LogDecodeResult fromIntegerTable(IntegerTableData data) {
            String payload = StreamEx.of(data.csv())
                    .nonNull()
                    .map(l -> IntStreamEx.of(l).joining(";"))
                    .joining("\n");
            return new LogUtils.LogDecodeResult(data.name(), payload);
        }
    }
}
