package com.gitlab.rcktsci.hackatons.x5.manager.analysis;

import lombok.Data;

@Data
public class SimpleMeasure {

    private final int initialAmount;
    private final int initialTick;
    private int finishTick;
    private int finishAmount;

    public final double calculateSellRate() {
        int deltaCount = calcDeltaAmount();
        int deltaTime = calcDeltaTime();
        return deltaTime > 0
                ? Math.abs(1.0 * deltaCount / deltaTime)
                : .0;
    }

    public int calcDeltaAmount() {
        return finishAmount - initialAmount;
    }

    public int calcDeltaTime() {
        return finishTick - initialTick;
    }
}
