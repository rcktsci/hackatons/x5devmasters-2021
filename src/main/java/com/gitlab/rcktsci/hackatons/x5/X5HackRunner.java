package com.gitlab.rcktsci.hackatons.x5;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.ApiException;
import com.gitlab.rcktsci.hackatons.x5.codegen.api.PerfectStoreEndpointApi;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentTickRequest;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentWorldResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Profile("!test")
@Component
@RequiredArgsConstructor
@Slf4j
public class X5HackRunner implements CommandLineRunner {

    private final PerfectStoreEndpointApi apiClient;
    private final List<NamedFeature> namedFeatures;
    private final List<SetupAtZero> initializers;
    private final List<EachTickExecutor> executors;
    private final List<AtTheEndLogger> loggers;

    @Override
    public void run(String... args) {
        printFeatures();
        try {
            CurrentWorldResponse world = awaitServer();
            while (!world.isGameOver()) {
                world = WorldContext
                        .executeWithinWorldContext(world, this::executeTick);
            }

            print(world);
        } catch (Exception ex) {
            log.error("{} @ {}", ex.getMessage(), getWhere(ex));
        }
    }

    private void printFeatures() {
        String output = StreamEx.of(namedFeatures)
                .map(NamedFeature::getName)
                .joining(", ");
        log.info("Features: {}", output);
    }

    @SneakyThrows
    private CurrentWorldResponse awaitServer() {
        long t0 = System.currentTimeMillis();
        //noinspection MagicNumber
        while (System.currentTimeMillis() - t0 < 10_000L) {
            try {
                CurrentWorldResponse world = apiClient.loadWorld();
                log.info("Подключение к серверу успешно. Начинаем игру.");
                return world;
            } catch (ApiException ex) {
                //noinspection BusyWait
                Thread.sleep(1000);
            }
        }

        throw new IllegalStateException("Cannot connect to server.");
    }

    @SneakyThrows
    private CurrentWorldResponse executeTick() {
        CurrentWorldResponse world = WorldContext.get();
        try {
            CommandsContext.reset();
            if (world.getCurrentTick() == 0) {
                initializers.forEach(SetupAtZero::setup);
            }
            executors.forEach(EachTickExecutor::tick);
            CommandsContext commandsContext = CommandsContext.get();
            CurrentTickRequest request = commandsContext.toRequest();
            return apiClient.tick(request);
        } catch (Exception ex) {
            log.error("{}) {} @ {}", world.getCurrentTick(), ex.getMessage(), getWhere(ex));
            CurrentTickRequest request = new CurrentTickRequest();
            return apiClient.tick(request);
        }
    }

    private static String getWhere(Exception ex) {
        String packageName = X5HackApplication.class.getPackageName();
        return StreamEx.of(ex.getStackTrace())
                .mapToEntry(StackTraceElement::getClassName)
                .filterValues(cn -> cn.startsWith(packageName))
                .keys()
                .map(ste -> "%s:%d".formatted(
                        ClassUtils.getShortClassName(ste.getClassName()),
                        ste.getLineNumber()))
                .findFirst()
                .orElse(StringUtils.EMPTY);
    }

    private void print(CurrentWorldResponse world) {
        // Печатаем собранную статистику.
        WorldContext.executeWithinWorldContext(world,
                () -> loggers.forEach(AtTheEndLogger::print));
    }
}
