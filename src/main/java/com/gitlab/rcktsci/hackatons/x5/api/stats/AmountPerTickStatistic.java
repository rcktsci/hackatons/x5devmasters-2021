package com.gitlab.rcktsci.hackatons.x5.api.stats;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class AmountPerTickStatistic extends ListPerTickStatistic {

    private static final int LOD = 10;

    public AmountPerTickStatistic(String name) {
        super(name, ReduceMode.LAST, LOD);
    }

    public AmountPerTickStatistic(String name, ReduceMode reduceMode) {
        super(name, reduceMode, LOD);
    }

    public void add(int tick, int amount) {
        put(tick, List.of(amount));
    }
}
