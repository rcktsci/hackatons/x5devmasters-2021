package com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SellRateResult {

    /**
     * ID продукта.
     */
    int productId;

    /**
     * Видимость (ранг) полки.
     */
    int rackCellRank;

    /**
     * Потенциальное кол-во продаж в 1 минуту.
     */
    double sellRate;
}
