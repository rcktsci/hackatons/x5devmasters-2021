package com.gitlab.rcktsci.hackatons.x5.manager.monitors;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class WorldPrinter implements AtTheEndLogger {

    @Override
    public void print() {
        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        String products = EntryStream.of(stockMap)
                .values()
                .sortedBy(Product::getId)
                .map(WorldPrinter::productToString)
                .joining("\n");
        log.info("Данные о продуктах:\n{}", products);
    }

    private static String productToString(Product product) {
        double sellPrice = Optional.of(product)
                .map(Product::getSellPrice)
                .orElse(0.0);
        double stockPrice = product.getStockPrice();
        return "#%2d - %s, %.2f / %.2f -> %.2f %%".formatted(
                product.getId(), product.getName(),
                sellPrice, stockPrice,
                100 * sellPrice / stockPrice);
    }
}
