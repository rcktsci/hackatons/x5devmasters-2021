package com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentWorldResponse;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.PutOffRackCellCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.PutOnRackCellCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.apache.commons.collections4.ListUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Order(1)
@ConditionalOnMissingBean(FixedSellRateAnalyzer.class)
@Service
@RequiredArgsConstructor
@Slf4j
public class SellRateAnalyzerImpl implements NamedFeature, SellRateAnalyzer, EachTickExecutor, AtTheEndLogger {

    public static final int ANALYZING_PERIOD = 7 * Values.DAY;
    private static final int INITIAL_AMOUNT = 2000;

    // Список участвующих продуктов.
    private final List<Integer> usingProducts = new ArrayList<>();
    // Группировка продуктов в группы таким же размером, сколько полок одного ранга в зале.
    private final List<List<IntermediateResult>> productGroups = new ArrayList<>();
    // Список всех необходимых к проведению экспериментов.
    private final List<RackExperiment> experimentPlan = new ArrayList<>();

    private int maxExperimentDuration = ANALYZING_PERIOD / 15;

    private final Map<Integer, IntermediateResult> analysisResultsByProductId = new HashMap<>();

    @Override
    public boolean isInAnalyzingPeriod() {
        int currentTick = WorldContext.getCurrentTick();
        if (ANALYZING_PERIOD <= currentTick) {
            // Вышли за максимальные рамки.
            return false;
        }

        if (currentTick < 10) {
            // Период разогрева.
            // Можно любое небольшое число, в общем-то, почему бы не 10?
            return true;
        }

        // Ещё имеются незаконченные эксперименты.
        boolean inProgress = StreamEx.of(experimentPlan)
                .anyMatch(e -> !e.isFinished());
        if (!inProgress) {
            log.info("{}) Досрочный выход из оценки.", currentTick);
        }

        return inProgress;
    }

    @Override
    public void tick() {
        if (!isInAnalyzingPeriod()) {
            return;
        }

        int currentTick = WorldContext.getCurrentTick();
        int tickOfAnalyzingPeriod = currentTick % maxExperimentDuration;
        if (tickOfAnalyzingPeriod == 0) {
            if (currentTick == 0) {
                // Начало игры — инициализируем структуры.
                setupExperiments();
                // Закупка начального количества.
                initialSupplement();
                return;
            } else {
                // Ничего не делаем на остальных нулевых тиках временных слотов.
            }
        }

        // На каждом первом тике временного слота начинаем новую серию экспериментов.
        if (tickOfAnalyzingPeriod == 1) {
            startExperiments();
            return;
        }

        // Последний тик временного слота — окончание серии экспериментов.
        if ((tickOfAnalyzingPeriod + 1) % maxExperimentDuration == 0) {
            // Это последний тик периода для серии экспериментов.
            stopExperimentSeries();
            return;
        }

        // Пытаемся остановить часть экспериментов досрочно.
        List<PutOffRackCellCommand> putOffRack = tryStopSomeExperiments();

        // Если мы остановили какие-либо эксперименты досрочно, просто на
        // всякий случай не будем в этот тик пополнять полки.
        if (putOffRack.isEmpty()) {
            CommandsContext.addOnRack(refillRacksFromStock());
        } else {
            CommandsContext.addOffRack(putOffRack);
        }
    }

    private void initialSupplement() {
        StreamEx.of(usingProducts)
                .map(id -> CmdFactory.buy(id, INITIAL_AMOUNT))
                .forEach(CommandsContext::addBuy);
    }

    // На момент начала серии экспериментов на всех полках пусто.
    private void startExperiments() {
        CurrentWorldResponse world = WorldContext.get();
        // Нужно запустить по одному эксперименту на каждый ранг полок.
        Set<Integer> productInSeries = new HashSet<>();
        List<PutOnRackCellCommand> result = IntStreamEx.range(1, 1 + 5)
                .boxed()
                // Выбираем любой новый эксперимент, главное без пересечения по продуктам.
                .mapPartial(rcr -> StreamEx.of(experimentPlan)
                        .filter(e -> !e.isStarted())
                        .filter(e -> e.getRackCellRank() == rcr)
                        .mapToEntry(RackExperiment::getProductGroupId)
                        .mapValues(productGroups::get)
                        .nonNullValues()
                        .filterValues(p -> StreamEx.of(p)
                                .map(IntermediateResult::getProduct)
                                .map(Product::getId)
                                .noneMatch(productInSeries::contains))
                        .keys()
                        .findFirst())
                .mapToEntry(RackExperiment::getProductGroupId)
                .mapValues(productGroups::get)
                .peekValues(ir -> StreamEx.of(ir)
                        .map(IntermediateResult::getProduct)
                        .map(Product::getId)
                        .forEach(productInSeries::add))
                .flatMapKeyValue((k, v) -> startExperiment(k, v, world).stream())
                .toList();
        // log.info("startExperiments(): {} штук.", result.size());
        CommandsContext.addOnRack(result);
    }

    private List<PutOnRackCellCommand> startExperiment(RackExperiment experiment, List<IntermediateResult> data, CurrentWorldResponse world) {
        int nextTick = world.getCurrentTick() + 1;
        experiment.setStarted(true);
        experiment.setStartedAt(nextTick);

        List<PutOnRackCellCommand> result = new ArrayList<>();

        int rackCellRank = experiment.getRackCellRank();
        List<RackCell> rackCellsGroup = StreamEx.of(world.getRackCells())
                .filter(rc -> rc.getVisibility() == rackCellRank)
                .toList();

        Map<Integer, Product> productsInStock = getUsedStock(world);

        for (int idx = 0; idx < data.size(); idx += 1) {
            RackCell rackCell = rackCellsGroup.get(idx);
            IntermediateResult intermediateResult = data.get(idx);
            Product product = productsInStock.get(intermediateResult.getProduct().getId());

            int amountInStock = product.getInStock();
            var measure = new IntermediateResult.MeasureData(
                    rackCellRank,
                    amountInStock,
                    nextTick);
            var measures = intermediateResult.getMeasures();
            measures.add(measure);

            int quantity = Math.min(rackCell.getCapacity(), amountInStock);
            if (quantity != 0) {
                PutOnRackCellCommand cmd = CmdFactory.toRackCell(
                        rackCell.getId(),
                        product.getId(),
                        quantity
                );
                result.add(cmd);
            }
        }

        return result;
    }

    private void stopExperimentSeries() {
        CurrentWorldResponse world = WorldContext.get();
        // Просто все полки, сгруппированные по рангу.
        Map<Integer, List<RackCell>> racksByRank = groupRacksByRank(world);
        // Активные эксперименты, идущие на каждом из рангов полок.
        Map<Integer, RackExperiment> experimentsByRank = findActiveExperimentsByRank(racksByRank.keySet());

        // Пробежка по всем активным экспериментам.
        EntryStream.of(experimentsByRank)
                .values()
                // Помечаем эксперимент как завершённый.
                .forEach(e -> finishExperiment(e, world));

        // Освобождаем вообще все полки в магазине.
        List<PutOffRackCellCommand> result = StreamEx.of(world.getRackCells())
                .filter(rc -> rc.getProductId() != null && rc.getProductId() != 0)
                .map(CmdFactory::fromRackCell)
                .toList();

        // log.info("{}) Освобождаем {} полок.", world.getCurrentTick(), result.size());
        CommandsContext.addOffRack(result);
    }

    private List<PutOffRackCellCommand> tryStopSomeExperiments() {
        CurrentWorldResponse world = WorldContext.get();
        // Просто все полки, сгруппированные по рангу.
        Map<Integer, List<RackCell>> racksByRank = groupRacksByRank(world);
        // Активные эксперименты, идущие на каждом из рангов полок.
        Map<Integer, RackExperiment> experimentsByRank = findActiveExperimentsByRank(racksByRank.keySet());
        // Текущее состояние товаров на складе.
        Map<Integer, Product> stockByProductId = getUsedStock(world);

        // Пробежка по всем активным экспериментам.
        return EntryStream.of(experimentsByRank)
                .invert()
                .mapValues(racksByRank::get)
                // Эксперимент должен закончиться досрочно, если на всех полках пусто и пополнить нечем.
                .filterValues(racks -> isExperimentStopsEarlier(racks, stockByProductId))
                // Помечаем эксперимент как завершённый.
                .peekKeys(e -> finishExperiment(e, world))
                // Освобождаем все участвующие в эксперименте полки.
                .values()
                .flatMap(Collection::stream)
                .map(CmdFactory::fromRackCell)
                .toList();
    }

    private void finishExperiment(RackExperiment experiment, CurrentWorldResponse world) {
        int currentTick = WorldContext.getCurrentTick();
        experiment.setFinished(true);
        experiment.setFinishedAt(currentTick);

        Map<Integer, Product> stock = WorldContext.getStockMap();

        int productGroupId = experiment.getProductGroupId();
        var productGroup = productGroups.get(productGroupId);
        for (IntermediateResult prr : productGroup) {
            int productId = prr.getProduct().getId();

            var measures = prr.getMeasures();
            var measure = measures.get(measures.size() - 1);
            measure.setFinished(true);
            measure.setFinishTick(currentTick);

            int inStock = stock.get(productId).getInStock();
            int onRack = StreamEx.of(world.getRackCells())
                    .findFirst(rc -> rc.getProductId() != null && rc.getProductId() == productId)
                    .map(RackCell::getProductQuantity)
                    .orElse(0);
            measure.setFinishAmount(inStock + onRack);
        }
    }

    private Map<Integer, RackExperiment> findActiveExperimentsByRank(Collection<Integer> rackCellRanks) {
        return StreamEx.of(rackCellRanks)
                .mapPartial(rackCellRank -> StreamEx.of(experimentPlan)
                        .filter(e -> e.getRackCellRank() == rackCellRank)
                        .filter(e -> e.isStarted() && !e.isFinished())
                        .findFirst())
                .mapToEntry(RackExperiment::getRackCellRank, e -> e)
                .toMap();
    }

    /**
     * Эксперимент заканчивается досрочно, так как больше нечего продавать.
     */
    private static boolean isExperimentStopsEarlier(Collection<RackCell> racks, Map<Integer, Product> stock) {
        // На всех полках группы пусто?
        boolean racksAreEmpty = StreamEx.of(racks)
                .allMatch(r -> defaultIfNull(r.getProductQuantity(), 0) == 0);
        boolean noProductsInStock = StreamEx.of(racks)
                .map(RackCell::getProductId)
                .nonNull()
                .filter(pid -> pid != 0)
                .map(stock::get)
                .allMatch(s -> defaultIfNull(s.getInStock(), 0) == 0);
        return racksAreEmpty && noProductsInStock;
    }

    private static Map<Integer, List<RackCell>> groupRacksByRank(CurrentWorldResponse world) {
        return StreamEx.of(world.getRackCells())
                .mapToEntry(RackCell::getVisibility, rc -> rc)
                .sorted(Map.Entry.comparingByKey())
                .collapseKeys()
                .toMap();
    }

    private static List<PutOnRackCellCommand> refillRacksFromStock() {
        CurrentWorldResponse world = WorldContext.get();
        // Доступно на складе для выкладки на полки.
        Map<Integer, Product> inStockById = StreamEx.of(world.getStock())
                .mapToEntry(Product::getId, p -> p)
                .filterValues(p -> p.getInStock() != null && p.getInStock() > 0)
                .toMap();

        return StreamEx.of(world.getRackCells())
                .filter(rc -> rc.getProductId() != null && rc.getProductId() != 0)
                .mapToEntry(RackCell::getProductId)
                .mapValues(inStockById::get)
                .nonNullValues()
                .mapKeyValue(CmdFactory::toRackCell)
                .toList();
    }

    private Map<Integer, Product> getUsedStock(CurrentWorldResponse world) {
        return StreamEx.of(world.getStock())
                .mapToEntry(Product::getId, p -> p)
                .filterKeys(usingProducts::contains)
                .toMap();
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public void setupExperiments() {
        CurrentWorldResponse world = WorldContext.get();
        List<Product> stock = new ArrayList<>(world.getStock());

        int productsCount = stock.size(); // Всегда 46 ?
        int racksCount = world.getRackCells().size(); // Всегда 15 ?
        int distinctRackRanks = 5; // Всегда 5 !
        int sameRankRacks = racksCount / distinctRackRanks; // Всегда 3 ?

        // Просто забудем про случайные продукты (один).
        int productsInGroup = sameRankRacks;
        int productsGroupsCount = productsCount / productsInGroup; // 46 / 3 = 15.
        int luckyProducts = productsGroupsCount * productsInGroup; // 15 * 3 = 45.

        // Формируем группы продуктов.
        Collections.shuffle(stock);
        List<IntermediateResult> randomProducts = StreamEx.of(stock)
                .limit(luckyProducts)
                .mapToEntry(Product::getId, IntermediateResult::new)
                .peekKeys(usingProducts::add)
                .peekKeyValue(analysisResultsByProductId::put)
                .values()
                .toList();
        productGroups.addAll(ListUtils.partition(randomProducts, productsInGroup));

        // Считаем, сколько максимум должны длиться эксперименты.
        this.maxExperimentDuration = ANALYZING_PERIOD / productsGroupsCount; // 1440 / 15 = 96 минут.

        // Составляем план тестирования.
        List<Integer> productGroupIds = IntStreamEx.range(productGroups.size())
                .boxed()
                .toList();
        IntStreamEx.range(1, 1 + distinctRackRanks)
                .boxed()
                .cross(productGroupIds)
                .mapKeyValue(RackExperiment::new)
                .forEach(experimentPlan::add);

        // 15 групп * 5 рангов = 75 ?
        int experimentsCount = experimentPlan.size();
        log.debug("Запланировано проведение {} экспериментов по {} тиков.", experimentsCount, maxExperimentDuration);
    }

    /**
     * Анализ проведённых экспериментов.
     */
    @Override
    public List<SellRateResult> calculateFinalRates() {
        List<SellRateResult> result = StreamEx.of(usingProducts)
                .mapToEntry(analysisResultsByProductId::get)
                .nonNullValues()
                .flatMapKeyValue(SellRateAnalyzerImpl::calculateProductRates)
                .reverseSorted(Comparator.comparing(SellRateResult::getSellRate))
                .toList();
        String json = LogUtils.toJson(result);
        String base64 = LogUtils.encodeGzipString(json);
        log.info("calculateFinalRates(): {}", base64);
        return result;
    }

    private static Stream<SellRateResult> calculateProductRates(int productId, IntermediateResult data) {
        return StreamEx.of(data.getMeasures())
                .map(m -> SellRateResult.builder()
                        .productId(productId)
                        .rackCellRank(m.getRackCellVisibility())
                        .sellRate(m.calculateSellRate())
                        .build());
    }

    @Override
    public void print() {
        calculateFinalRates();
    }
}
