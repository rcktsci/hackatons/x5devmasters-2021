package com.gitlab.rcktsci.hackatons.x5.a_logs;

import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class Base64ToJSON {

    @SneakyThrows
    public static void main(String[] args) {
        String base64 = LogUtils.readClipboardBase64();
        String json = LogUtils.decodeGzipString(base64);
        log.info("Decoded JSON: {}", json);
    }
}
