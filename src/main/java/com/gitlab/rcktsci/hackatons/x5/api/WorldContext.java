package com.gitlab.rcktsci.hackatons.x5.api;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.CheckoutLine;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentWorldResponse;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Customer;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Employee;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.springframework.lang.NonNull;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@UtilityClass
@Slf4j
public class WorldContext {

    private final ThreadLocal<CurrentWorldResponse> context = new ThreadLocal<>();

    private static final Comparator<RackCell> RACK_CELL_RANK_COMPARATOR = Comparator
            .comparing(RackCell::getVisibility)
            .thenComparing(RackCell::getCapacity)
            .reversed()
            .thenComparing(RackCell::getId);

    @SneakyThrows
    public <T> T executeWithinWorldContext(CurrentWorldResponse world, Supplier<T> supplier) {
        try {
            context.set(world);
            return supplier.get();
        } finally {
            context.remove();
        }
    }

    @SneakyThrows
    public void executeWithinWorldContext(CurrentWorldResponse world, Runnable runnable) {
        try {
            context.set(world);
            runnable.run();
        } finally {
            context.remove();
        }
    }

    @NonNull
    public CurrentWorldResponse get() {
        return Optional.ofNullable(context.get())
                .orElseThrow();
    }

    public int getCurrentTick() {
        return get().getCurrentTick();
    }

    public int getTickCount() {
        return get().getTickCount();
    }

    public int getTicksUntilEnd() {
        CurrentWorldResponse world = get();
        return world.getTickCount() - world.getCurrentTick();
    }

    public int getTickOfDay() {
        return Values.toDayTick(getCurrentTick());
    }

    public Map<Integer, Customer> getCustomerMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getCustomers())
                .mapToEntry(Customer::getId, c -> c)
                .toMap();
    }

    public List<Customer> getCustomersInStatus(Customer.ModeEnum mode) {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getCustomers())
                .filter(c -> c.getMode() == mode)
                .toList();
    }

    public Map<Integer, Employee> getEmployeeMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getEmployees())
                .mapToEntry(Employee::getId, e -> e)
                .toMap();
    }

    public Map<Integer, CheckoutLine> getCheckoutLineMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getCheckoutLines())
                .mapToEntry(CheckoutLine::getId, cl -> cl)
                .toMap();
    }

    public int getRackCellRank(int rackCellId) {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getRackCells())
                .filterBy(RackCell::getId, rackCellId)
                .findFirst()
                .map(RackCell::getVisibility)
                .orElseThrow();
    }

    public List<RackCell> getSortedRacks() {
        CurrentWorldResponse world = get();
        // Сперва идут самые заметные и самые объёмные.
        return StreamEx.of(world.getRackCells())
                .sorted(RACK_CELL_RANK_COMPARATOR)
                .toList();
    }

    public Map<Integer, RackCell> getRackCellMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getRackCells())
                .mapToEntry(RackCell::getId, rc -> rc)
                .toMap();
    }

    public Map<Integer, RackCell> getAssignedRackCellMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getRackCells())
                .filter(rc -> rc.getProductId() != null && rc.getProductId() > 0)
                .mapToEntry(RackCell::getId, rc -> rc)
                .toMap();
    }

    public Map<Integer, RackCell> getNonEmptyRackCellMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getRackCells())
                .filter(rc -> rc.getProductId() != null && rc.getProductId() > 0)
                .filter(rc -> rc.getProductQuantity() != null && rc.getProductQuantity() > 0)
                .mapToEntry(RackCell::getId, rc -> rc)
                .toMap();
    }

    public Map<Integer, Integer> getRackCellByProductMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getRackCells())
                .mapToEntry(RackCell::getProductId, RackCell::getId)
                .nonNullKeys()
                .toMap();
    }

    public Map<Integer, Product> getStockMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getStock())
                .mapToEntry(Product::getId, p -> p)
                .toMap();
    }

    public Map<Integer, Product> getNonEmptyStockMap() {
        CurrentWorldResponse world = get();
        return StreamEx.of(world.getStock())
                .filter(p -> p.getInStock() > 0)
                .mapToEntry(Product::getId, p -> p)
                .toMap();
    }

    public int getProductVolume(int productId) {
        CurrentWorldResponse world = get();
        int result = StreamEx.of(world.getStock())
                .filterBy(Product::getId, productId)
                .mapToInt(Product::getInStock)
                .findFirst()
                .orElse(0);
        List<RackCell> rackCells = world.getRackCells();
        for (RackCell rackCell : rackCells) {
            if (rackCell.getProductId() != null && rackCell.getProductId() == productId) {
                result += rackCell.getProductQuantity();
            }
        }
        return result;
    }

    public Map<Integer, Integer> sumProductsOnRackCellsAndInStock() {
        return EntryStream
                .of(getNonEmptyRackCellMap())
                .values()
                .mapToEntry(
                        RackCell::getProductId,
                        RackCell::getProductQuantity)
                .append(EntryStream
                        .of(getNonEmptyStockMap())
                        .mapValues(Product::getInStock))
                .sorted(Map.Entry.comparingByKey())
                .collapseKeys()
                .mapValues(l -> IntStreamEx.of(l).sum())
                .toMap();
    }

    public boolean hasNonEmptyVolumes() {
        CurrentWorldResponse world = get();
        for (RackCell rackCell : world.getRackCells()) {
            Integer quantity = rackCell.getProductQuantity();
            if (quantity != null && quantity > 0) {
                return true;
            }
        }

        for (Product product : world.getStock()) {
            if (product.getInStock() > 0) {
                return true;
            }
        }

        return false;
    }
}
