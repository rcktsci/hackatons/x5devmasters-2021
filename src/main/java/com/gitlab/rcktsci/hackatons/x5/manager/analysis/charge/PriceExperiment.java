package com.gitlab.rcktsci.hackatons.x5.manager.analysis.charge;

import com.gitlab.rcktsci.hackatons.x5.manager.analysis.SimpleMeasure;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceExperiment extends SimpleMeasure {

    private int productId;
    private int rackCellRank;
    private double priceMultiplier;
    private double weeklyProfit;

    public PriceExperiment(int initialAmount, int initialTick) {
        super(initialAmount, initialTick);
    }

    public String forLogLong() {
        return "product %d, mul = %f, income = %f"
                .formatted(productId, priceMultiplier, weeklyProfit);
    }

    public String forLog() {
        return "mul = %f (income = %f)"
                .formatted(priceMultiplier, weeklyProfit);
    }
}
