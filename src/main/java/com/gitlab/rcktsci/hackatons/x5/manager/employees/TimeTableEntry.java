package com.gitlab.rcktsci.hackatons.x5.manager.employees;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.HireEmployeeCommand.ExperienceEnum;
import lombok.Data;

@Data
public class TimeTableEntry {

    private final int scheduleId;

    private final ExperienceEnum experience;
    private final int checkoutId;
    private final int startAtDayTick;

    private Integer employeeId;
}
