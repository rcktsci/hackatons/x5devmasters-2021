package com.gitlab.rcktsci.hackatons.x5.api;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface SetupAtZero {

    void setup();
}
