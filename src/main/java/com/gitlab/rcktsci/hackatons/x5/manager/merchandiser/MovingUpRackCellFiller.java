package com.gitlab.rcktsci.hackatons.x5.manager.merchandiser;

import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentWorldResponse;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.IntStreamEx;

import java.util.List;
import java.util.Map;
import java.util.Objects;

// @Service
@RequiredArgsConstructor
@Slf4j
public class MovingUpRackCellFiller implements NamedFeature, EachTickExecutor {

    @Override
    public void tick() {
        CurrentWorldResponse world = WorldContext.get();
        List<RackCell> rackCells = world.getRackCells();

        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        Map<Integer, Integer> rackCellByProductMap = WorldContext.getRackCellByProductMap();

        Mode mode = Mode.REFILL;

        for (RackCell rackCell : rackCells) {
            switch (mode) {
                case REFILL -> {
                    Integer productId = rackCell.getProductId();
                    if (productId == null) {
                        // Находим первый продукт на складе, который
                        // имеется в наличии и ещё не выложен ан полки.
                        IntStreamEx.range(1, 46)
                                .boxed()
                                .mapToEntry(stockMap::get, rackCellByProductMap::get)
                                .filterValues(Objects::isNull)
                                .keys()
                                .filter(p -> p.getInStock() > 0)
                                .findFirst()
                                .ifPresent(p -> {
                                    var cmd = CmdFactory.toRackCell(rackCell, p);
                                    CommandsContext.addOnRack(cmd);
                                });
                    } else {
                        Product product = stockMap.get(productId);
                        if (product.getInStock() > 0) {
                            // Простое пополнение содержимого полки.
                            var cmd = CmdFactory.toRackCell(rackCell, product);
                            CommandsContext.addOnRack(cmd);
                        } else {
                            // Освобождаем текущую полку и все товары со следующих.
                            var cmd = CmdFactory.fromRackCell(rackCell);
                            CommandsContext.addOffRack(cmd);
                            mode = Mode.REMOVE;
                        }
                    }
                }
                case REMOVE -> {
                    // Снимаем все товары с полок.
                    var cmd = CmdFactory.fromRackCell(rackCell);
                    CommandsContext.addOffRack(cmd);
                }
            }
        }
    }

    private enum Mode {
        REFILL,
        REMOVE,
        ;
    }
}
