package com.gitlab.rcktsci.hackatons.x5.manager.supplier;

import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.BruteForceService;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.BruteForceService.BruteForceResult;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement.SellRateAnalyzer;
import com.gitlab.rcktsci.hackatons.x5.manager.precalc.InitialData;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Order(10)
@Service
@RequiredArgsConstructor
@Slf4j
public class GenericSupplier implements NamedFeature, ProductsSupplier, EachTickExecutor {

    private final SellRateAnalyzer sellRateAnalyzer;
    private final BruteForceService bruteForceService;
    private boolean executed;

    @Override
    public void tick() {
        if (executed || sellRateAnalyzer.isInAnalyzingPeriod()) {
            return;
        }
        executed = true;

        if (InitialData.EXPERIMENT_MODE) {
            log.info("Брутфорс данных от SellRateAnalyzer.");
            List<BruteForceResult> data = bruteForceService.bruteForce(
                    sellRateAnalyzer.calculateFinalRates(),
                    false);
            buyPartially(data);
        } else {
            log.info("Получены данные от InitialData.precalc().");
            List<BruteForceResult> data = InitialData.precalc();
            buyPartially(data);
        }
    }

    private static void buyPartially(Collection<BruteForceResult> data) {
        StreamEx.of(data)
                .mapToEntry(
                        BruteForceResult::productId,
                        BruteForceResult::amountToSell)
                .mapValues(amount -> amount / 2)
                .mapKeyValue(CmdFactory::buy)
                .forEach(CommandsContext::addBuy);
    }

    @Override
    public void resupply(Map<Integer, Integer> currentNeeds) {
        EntryStream.of(currentNeeds)
                .nonNullKeys()
                .nonNullValues()
                .mapValues(amount -> 5 * amount / 12)
                .mapKeyValue(CmdFactory::buy)
                .forEach(CommandsContext::addBuy);
    }
}
