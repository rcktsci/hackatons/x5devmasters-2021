package com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.SimpleMeasure;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Data
public class IntermediateResult {

    private final Product product;
    private final List<MeasureData> measures = new ArrayList<>();

    @Getter
    @Setter
    public static class MeasureData extends SimpleMeasure {

        private final int rackCellVisibility;

        private boolean isFinished;

        public MeasureData(int rackCellVisibility, int initialAmount, int initialTick) {
            super(initialAmount, initialTick);
            this.rackCellVisibility = rackCellVisibility;
        }
    }
}
