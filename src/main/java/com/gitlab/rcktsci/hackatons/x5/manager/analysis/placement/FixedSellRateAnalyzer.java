package com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement;

import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Order(1)
@Service
@RequiredArgsConstructor
@Slf4j
public class FixedSellRateAnalyzer implements NamedFeature, SellRateAnalyzer {

    // @formatter:off
    private static final String DATA = "H4sIAAAAAAAC/6Vau45cNwz9l60Ng0+RdJsqbdoghWG7spEEjlMF+fdoZndn7qWCPRBmuwXuocT3ITW//vP05/c/Pv/96cfPn58+KL97+v7x09efvnz79svH378+ffB3T39d//nx5ekDv3ceQRo5uNh1xL/vznjpeDvjzYq1JInFuXzBGzjfzE1NKs2osqENnq5qWSmRPki8wWUguIQokY2yVB/L8QNcXjN8nq01TIfL6MrTm3h6n0Jj4klGkad2+EBwtmEZUjY10MX2XAAfI5WpmJNyjOrX5+x4bfjbyWI1Fel4dP8gI/UMMy2bAQj1tzN+VNL0GvEMvGmLfr4ZOH+q7ENIJdOKpLtfMN5phAzz0CqG3rcdOGPtXw0/lKIU5k6//Ux6sWSdeV+DBSZPx7c/WDkW/Cw3wZf8szRlGH0d78Wz8Myyw8IU/XxjhLeZ/HazQtff6DG8wPu/+J10FlDi7j9R5P+38Wv2L3gJn6VfRwlFJIw/3cKrQv1ny5hl2zWZpzM7HtrvFD5kHR9If51tj2Noyhg0+vkC4/deuaW8cOfr53OolrIYDY8OFxh+7HlpnrNt+hje00dg+PPsuxXhkRau3XwSCD/b9XALShfiKNg7u/qkmUMrBqUGLeZjiKeska/X6PB6CL4mTwt+r0lalLhm4apcOgfqvK10KXR+u71n1iQfPNlXyUo8GDnPoyqIJ/N5TgKYOwv+1HuX2ucQf6xc5rD27eEN1c4Da7gwKIV438JzwPNnxgZP6qru4Q7tt+CdREzYxS59FMbfgtdJXIersIYvzGstHrqF1wHxt7J/TYRufxg/02iWtyhe4hfaz8296Do+SDKsXbIDXzv/cjr7gfwZJI7degfVZwIntF4/3+RY/ga0nm/h186pe/hA+GPozBoKmUfXX50lyF3pSmIgc+x4qTlt1G16g8xxwSuPLMqcQ7cqJA5dfZkzu7GRU4owJv49eq/9WkO4QqkMjk39+pwj7d6/IXHv578Z/TIegissXWxCJJXlOoYwHPoW7Y+hs9JOhec/93xR5dQckHbzFt5g8bgGjYgHE7Fh3raH14T4w8xL69gF/Uen9DF4vm3hJR7DK8y++8gUEuUw+xf8sfEHJr6L/SgsNI2fuxiknu38F8b5On0wzL8Ff5s5dKgobJ7a8Sqh9xZisPtYF8BzWo0wyar0ZfIQVH/vOxPWQWKQPPULHFrHHB8HJH/dgoMz1SgpOCwLkr9+/zZ6YfbZBRzJD/HSwE2hgNvY+kyCIQPqJji3YNjBlvPPTQASoI5vSQAJ2II/Le8S5rBs4dfxacFfF66vUYRXf7yHRz3INGoEs1MFexXeHfKmAJhDeq5D0AO8hw94/i17zGQZYDmgBS9rBzLOZzILB7DlfDoW0oL7j45/KZ4vvZBhBPb7n3aHIZhEdgccti8XMwRkwYuAew+9bhGhC7sKfH6/gEWwm/Cy9juuUWAMbAowmAR0JnMBO9mugIQCDu8/l3ICe3ETcF4DCe6FC/6li2vxkPURBVXCZ5hU2PNKziEd7wJOS9D/ecNEUXBcA176UcCBZBFw3X9nvOw0FRKqTQGKcvm0h89loFwp8R5+DeMFf7W8MdNgSccLgW6BA5+/DOeQjyz481sS5kO8J4DrQQEKbXjO44XVKsyk42OmrHMBDIJTP13mMkOUEuA1If78oKowkXlPgEEXNFYUsB8sAl5niowsWdbKaynpAu7vWZe+hBeDC35OhJKj6rIixa8SC/yyEbnvxvFmqwsQT6/7gBiQ1uwKgKXwrv51yYVZRRdw5oXrcAdLydmGndszGq8BXmAQHlejtL7tKBRwrR8SQhY5HRKQVy0C2o4Bkoom4PyzmCWRGe3Hz9yaFm7LqCOfNpS67EgZ5eHpZ0myGACtaE7riVlKlp8WIBe2FZksQYhK4Wk0uGRCDyKgwflx+PLE3W+A2glBAO8CbBeguwB/G+CbB/imBr5pIt/0gW/qy/YwgHcBsgvQXQBwgmzqLJsayKaJ5NH7AwONTX3Ho/Jt83ve/B7YfzdE7VH5uvm9bX6PclJ3U0x3k1h3K7XuJrFuKl2bOtemyrWpcW0qXLtO3vXArgN2Y2jTnrmpbz4qH9x/N952U2Y7J7db9y5X4V2ywrx7pe2gw1H923+XYiF+7jEAAA==";
    // @formatter:on

    @Override
    public boolean isInAnalyzingPeriod() {
        return false;
    }

    /**
     * Анализ проведённых экспериментов.
     */
    @Override
    public List<SellRateResult> calculateFinalRates() {
        log.info("Используем измеренные константы скорости продажи товаров.");
        String json = LogUtils.decodeGzipString(DATA);
        SellRateResult[] data = LogUtils.fromJson(json, SellRateResult[].class);
        return Arrays.asList(data);
    }
}
