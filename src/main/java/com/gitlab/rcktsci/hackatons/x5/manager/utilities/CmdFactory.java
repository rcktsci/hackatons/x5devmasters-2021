package com.gitlab.rcktsci.hackatons.x5.manager.utilities;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.BuyStockCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CheckoutLine;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Employee;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.FireEmployeeCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.HireEmployeeCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.PutOffRackCellCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.PutOnRackCellCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetOffCheckoutLineCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetOnCheckoutLineCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetPriceCommand;
import lombok.experimental.UtilityClass;

/**
 * Фабрика команд серверу.
 */
@UtilityClass
public class CmdFactory {

    public HireEmployeeCommand hireCashier(HireEmployeeCommand.ExperienceEnum type) {
        HireEmployeeCommand result = new HireEmployeeCommand();
        result.setExperience(type);
        return result;
    }

    public HireEmployeeCommand hireCashier(HireEmployeeCommand.ExperienceEnum type, CheckoutLine checkout) {
        HireEmployeeCommand result = new HireEmployeeCommand();
        result.setExperience(type);
        result.setCheckoutLineId(checkout.getId());
        return result;
    }

    public FireEmployeeCommand fireCashier(Employee employee) {
        FireEmployeeCommand result = new FireEmployeeCommand();
        result.setEmployeeId(employee.getId());
        return result;
    }

    public SetOffCheckoutLineCommand cashierOff(Employee employee) {
        SetOffCheckoutLineCommand result = new SetOffCheckoutLineCommand();
        result.setEmployeeId(employee.getId());
        return result;
    }

    public SetOnCheckoutLineCommand cashierOn(Employee employee, CheckoutLine checkout) {
        SetOnCheckoutLineCommand result = new SetOnCheckoutLineCommand();
        result.setEmployeeId(employee.getId());
        result.setCheckoutLineId(checkout.getId());
        return result;
    }

    public BuyStockCommand buy(int productId, int quantity) {
        BuyStockCommand result = new BuyStockCommand();
        result.setProductId(productId);
        result.setQuantity(quantity);
        return result;
    }

    public SetPriceCommand price(int productId, double sellPrice) {
        SetPriceCommand result = new SetPriceCommand();
        result.setProductId(productId);
        result.setSellPrice(sellPrice);
        return result;
    }

    public PutOffRackCellCommand fromRackCell(RackCell r) {
        PutOffRackCellCommand result = new PutOffRackCellCommand();
        result.setRackCellId(r.getId());
        return result;
    }

    public PutOnRackCellCommand toRackCell(RackCell rack, Product product) {
        PutOnRackCellCommand result = new PutOnRackCellCommand();
        result.setRackCellId(rack.getId());
        result.setProductId(product.getId());

        int capacity = rack.getCapacity();
        int busySpace = rack.getProductQuantity() != null ? rack.getProductQuantity() : 0;
        int freeSpace = capacity - busySpace;
        int quantity = Math.min(freeSpace, product.getInStock());
        result.setProductQuantity(quantity);

        return result;
    }

    public PutOnRackCellCommand toRackCell(int rackCellId, int productId, int quantity) {
        PutOnRackCellCommand result = new PutOnRackCellCommand();
        result.setRackCellId(rackCellId);
        result.setProductId(productId);
        result.setProductQuantity(quantity);
        return result;
    }
}
