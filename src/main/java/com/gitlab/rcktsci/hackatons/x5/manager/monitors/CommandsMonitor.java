package com.gitlab.rcktsci.hackatons.x5.manager.monitors;

import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.BuyStockCommand;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentTickRequest;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.SetPriceCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Order(100)
@Component
@RequiredArgsConstructor
@Slf4j
public class CommandsMonitor implements EachTickExecutor {

    /**
     * Услуги доставки.
     */
    private static final int PAYMENT = 5000;

    @Override
    public void tick() {
        CurrentTickRequest currentTickRequest = CommandsContext.get().toRequest();
        printBuy(currentTickRequest);
        printPrice(currentTickRequest);
    }

    private static void printBuy(CurrentTickRequest request) {
        List<BuyStockCommand> buyStockCommands = request.getBuyStockCommands();
        if (buyStockCommands.isEmpty()) {
            return;
        }

        Map<Integer, Product> stock = WorldContext.getStockMap();
        double sum = StreamEx.of(buyStockCommands)
                .mapToEntry(BuyStockCommand::getProductId, BuyStockCommand::getQuantity)
                .mapKeys(stock::get)
                .mapKeys(Product::getStockPrice)
                .mapKeyValue((k, v) -> k * v)
                .mapToDouble(s -> s)
                .sum();

        sum += PAYMENT;

        log.info("{}) Закупка у поставщика на {} (с доставкой).", WorldContext.getCurrentTick(), BigDecimal.valueOf(sum).toPlainString());
    }

    private static void printPrice(CurrentTickRequest request) {
        List<SetPriceCommand> setPriceCommands = request.getSetPriceCommands();
        if (setPriceCommands.isEmpty()) {
            return;
        }

        for (SetPriceCommand command : setPriceCommands) {
            int currentTick = WorldContext.getCurrentTick();
            Integer productId = command.getProductId();
            Double sellPrice = command.getSellPrice();
            // log.info("{}) Продукт {}, новая цена {}", currentTick, productId, sellPrice);
        }
    }
}
