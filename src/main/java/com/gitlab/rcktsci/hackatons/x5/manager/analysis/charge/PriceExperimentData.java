package com.gitlab.rcktsci.hackatons.x5.manager.analysis.charge;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Value
@Builder
public class PriceExperimentData {

    @SerializedName("pid")
    int productId;

    @SerializedName("rcr")
    Map<Integer, Result> rackCellRank;

    @Value
    public static class Result {

        @SerializedName("mul")
        double priceMultiplier;

        @SerializedName("wp")
        double weeklyProfit;
    }
}
