package com.gitlab.rcktsci.hackatons.x5.manager.monitors;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.api.stats.AmountPerTickStatistic;
import com.gitlab.rcktsci.hackatons.x5.api.stats.ListPerTickStatistic;
import com.gitlab.rcktsci.hackatons.x5.api.stats.ReduceMode;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Customer;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.ProductInBasket;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientsMonitor implements EachTickExecutor, AtTheEndLogger {

    private final Collection<Integer> totalUniqueClientIds = new HashSet<>();
    private final AmountPerTickStatistic uniqueClientsTotal = new AmountPerTickStatistic("total-unique-clients");
    private final AmountPerTickStatistic clientsInHallNow = new AmountPerTickStatistic("clients-in-hall");
    private final AmountPerTickStatistic clientsWaitCheckoutNow = new AmountPerTickStatistic("clients-wait-checkout");
    private final AmountPerTickStatistic clientsAtCheckoutNow = new AmountPerTickStatistic("clients-at-checkout");

    private final ListPerTickStatistic productsPopularity = new AmountPerTickStatistic("products-popularity", ReduceMode.AVG);

    @Override
    public void tick() {
        int currentTick = WorldContext.getCurrentTick();
        Map<Integer, Customer> customerMap = WorldContext.getCustomerMap();
        totalUniqueClientIds.addAll(customerMap.keySet());
        uniqueClientsTotal.add(currentTick, totalUniqueClientIds.size());

        clientsInHallNow.add(currentTick, WorldContext.getCustomersInStatus(Customer.ModeEnum.IN_HALL).size());
        clientsWaitCheckoutNow.add(currentTick, WorldContext.getCustomersInStatus(Customer.ModeEnum.WAIT_CHECKOUT).size());
        clientsAtCheckoutNow.add(currentTick, WorldContext.getCustomersInStatus(Customer.ModeEnum.AT_CHECKOUT).size());

        Map<Integer, List<Integer>> productsInBaskets = EntryStream.of(customerMap)
                .values()
                .flatCollection(Customer::getBasket)
                .mapToEntry(ProductInBasket::getId, ProductInBasket::getProductCount)
                .grouping();

        List<Integer> productsSummed = EntryStream.of(WorldContext.getStockMap())
                .keys()
                .sorted()
                .mapToEntry(id -> productsInBaskets.getOrDefault(id, List.of()))
                .mapValues(l -> IntStreamEx.of(l).sum())
                .values()
                .toList();
        productsPopularity.put(currentTick, productsSummed);
    }

    @Override
    public void print() {
        // uniqueClientsTotal.print();
        // clientsInHallNow.print();
        // clientsWaitCheckoutNow.print();
        // productsPopularity.print();
    }
}
