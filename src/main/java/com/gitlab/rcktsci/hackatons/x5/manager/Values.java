package com.gitlab.rcktsci.hackatons.x5.manager;

import lombok.experimental.UtilityClass;

import java.time.LocalTime;

/**
 * Всякие захардкоженные параметры.
 */
@UtilityClass
public class Values {

    /**
     * 1 час в игровых тиках.
     */
    public final int HOUR = 60;

    /**
     * 1 день в игровых тиках.
     */
    public final int DAY = 24 * HOUR;

    /**
     * 1 неделя в игровых тиках.
     */
    public final int WEEK = 7 * DAY;

    /**
     * Период непрерывной работы кассира.
     */
    public final int EMPLOYEE_WORK_TICKS = 8 * HOUR;

    public int toDayTick(int globalTick) {
        return globalTick % DAY;
    }

    public int toDayTick(LocalTime timeOfDay) {
        return timeOfDay.getHour() * HOUR + timeOfDay.getMinute();
    }
}
