package com.gitlab.rcktsci.hackatons.x5.manager.employees;

import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CheckoutLine;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Employee;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.HireEmployeeCommand.ExperienceEnum;
import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class CheckoutLinesPlanner implements SetupAtZero, EachTickExecutor {

    private static final boolean HACK_INITIAL_HIRING = true;

    private static final List<TimeTableEntry> TIME_TABLE = List.of(
            new TimeTableEntry(0,
                    ExperienceEnum.SENIOR,
                    1,
                    Values.toDayTick(LocalTime.of(0, 0))),
            new TimeTableEntry(1,
                    ExperienceEnum.SENIOR,
                    1,
                    Values.toDayTick(LocalTime.of(8, 0))),
            new TimeTableEntry(2,
                    ExperienceEnum.SENIOR,
                    1,
                    Values.toDayTick(LocalTime.of(16, 0))),
            new TimeTableEntry(3,
                    ExperienceEnum.SENIOR,
                    2,
                    Values.toDayTick(LocalTime.of(0, 0))),
            new TimeTableEntry(4,
                    ExperienceEnum.SENIOR,
                    2,
                    Values.toDayTick(LocalTime.of(8, 0))),
            new TimeTableEntry(5,
                    ExperienceEnum.SENIOR,
                    2,
                    Values.toDayTick(LocalTime.of(16, 0)))
    );

    private final Collection<Integer> registeredEmployees = new HashSet<>();

    @Override
    public void setup() {
        if (HACK_INITIAL_HIRING) {
            for (int i = 0; i < 200; i++) {
                var cmd = CmdFactory.hireCashier(ExperienceEnum.SENIOR);
                CommandsContext.addHire(cmd);
            }
        }
    }

    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public void tick() {
        int currentTick = WorldContext.getCurrentTick();
        int tickOfDay = Values.toDayTick(currentTick);

        Map<Integer, Employee> employeesById = WorldContext.getEmployeeMap();
        Map<Integer, CheckoutLine> checkoutLineMap = WorldContext.getCheckoutLineMap();

        if (HACK_INITIAL_HIRING) {
            if (employeesById.isEmpty()) {
                // Это нулевой тик.
                return;
            }

            boolean employeesAreNotScheduled = StreamEx.of(TIME_TABLE)
                    .map(TimeTableEntry::getEmployeeId)
                    .allMatch(Objects::isNull);
            if (employeesAreNotScheduled) {
                // Тик #1 после найма over-множества сотрудников.
                selectEmployeesOfTheMonth(employeesById);
            }

            for (TimeTableEntry schedule : TIME_TABLE) {
                // Пропускаем не относящиеся к текущим времени и кассе слоты.
                int startsAt = schedule.getStartAtDayTick();
                int stopsAt = (startsAt + Values.EMPLOYEE_WORK_TICKS - 1) % Values.DAY;
                if (tickOfDay < startsAt || stopsAt < tickOfDay) {
                    continue;
                }

                // Может быть несколько случаев:
                // 1. Начало игры, на кассу никого не нанимали. Нужно нанять.
                // 2. Сотрудник нанят в текущем тике — нужно взять его ID и записать в структуры данных.
                // 3. Последний тик рабочего периода кассы, нужно попросить сотрудника сворачиваться.
                // 4. Первые тики нового периода. На кассе может тупить предыдущий сотрудник.
                CheckoutLine checkoutLine = checkoutLineMap.get(schedule.getCheckoutId());
                int checkoutLineId = checkoutLine.getId();

                Integer scheduledEmployeeId = schedule.getEmployeeId();
                Employee scheduledEmployee = scheduledEmployeeId != null
                        ? employeesById.get(scheduledEmployeeId)
                        : null;

                Integer checkoutLineEmployeeId = checkoutLine.getEmployeeId();
                Employee checkoutLineEmployee = checkoutLineEmployeeId != null
                        ? employeesById.get(checkoutLineEmployeeId)
                        : null;

                // Начнём с простого случая, конец текущего периода.
                if (tickOfDay == stopsAt && scheduledEmployee != null) {
                    // Снять сотрудника с линии.
                    // log.info("{}) Сотрудник {} сворачивается с кассы.", currentTick, EmployeeUtils.toPrettyString(scheduledEmployee));
                    CommandsContext.addFromCheckout(CmdFactory.cashierOff(scheduledEmployee));
                    continue;
                }

                // Если на кассе имеется сотрудник, нужно понять, это он только что вышел на неё, или это тупит предыдущий?
                if (checkoutLineEmployee != null) {
                    // Это просто работает тот, кто и должен работать на кассе.
                    // Давайте ничего с ним делать не будем?
                    if (checkoutLineEmployee.equals(scheduledEmployee)) {
                        continue;
                    }

                    // Это тупит предыдущий сотрудник? Тоже повлиять на него не можем.
                    if (registeredEmployees.contains(checkoutLineEmployeeId)) {
                        continue;
                    }

                    // Это вышел наш новенький? Пропишем его везде.
                    if (scheduledEmployee == null) {
                        schedule.setEmployeeId(checkoutLineEmployeeId);
                        registeredEmployees.add(checkoutLineEmployeeId);
                        // log.info("{}) Сотрудник {} вышел на кассу {}.", currentTick, EmployeeUtils.toPrettyString(checkoutLineEmployee), checkoutLineId);
                    }

                    // Дальше ничего обрабатывать не будем.
                    continue;
                }

                // На кассе никого нет.
                if (scheduledEmployee != null) {
                    // log.info("{}) Просим выйти {} на кассу {}.", currentTick, EmployeeUtils.toPrettyString(scheduledEmployee), checkoutLineId);
                    CommandsContext.addToCheckout(CmdFactory.cashierOn(scheduledEmployee, checkoutLine));
                } else {
                    // log.info("{}) Нанимаем {} на кассу {}.", currentTick, schedule.getExperience().name(), checkoutLineId);
                    CommandsContext.addHire(CmdFactory.hireCashier(schedule.getExperience(), checkoutLine));
                }
            }
        }
    }

    private static void selectEmployeesOfTheMonth(Map<Integer, Employee> employeesById) {
        int required = TIME_TABLE.size();

        List<Integer> hired = EntryStream.of(employeesById)
                .values()
                .mapToEntry(Employee::getExperience)
                .reverseSorted(Map.Entry.comparingByValue())
                .keys()
                .limit(required)
                .map(Employee::getId)
                .toList();

        // Сопоставляем кассиров кассам.
        EntryStream.of(TIME_TABLE)
                .invert()
                .mapValues(idx -> hired.get(required - 1 - idx))
                .forKeyValue(TimeTableEntry::setEmployeeId);

        // Увольняем всех остальных.
        EntryStream.of(employeesById)
                .filterKeys(eid -> hired.stream().noneMatch(eid::equals))
                .values()
                .map(CmdFactory::fireCashier)
                .forEach(CommandsContext::addFire);

        for (int employeeId : hired) {
            int currentTick = WorldContext.getCurrentTick();
            Employee employee = employeesById.get(employeeId);
            String pretty = EmployeeUtils.toPrettyString(employee);
            // log.info("{}) Нанят божественный работник {}.", currentTick, pretty);
        }
    }
}
