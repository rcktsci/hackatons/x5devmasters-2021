package com.gitlab.rcktsci.hackatons.x5;

import com.gitlab.rcktsci.hackatons.x5.codegen.ApiClient;
import com.gitlab.rcktsci.hackatons.x5.codegen.api.PerfectStoreEndpointApi;
import com.squareup.okhttp.OkHttpClient;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.annotation.Validated;

import java.util.concurrent.TimeUnit;

@EnableConfigurationProperties(value = X5HackApplication.HackAppProperties.class)
@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class X5HackApplication {

    @ConfigurationProperties("hack-app")
    @ConstructorBinding
    @Validated
    @Value
    static class HackAppProperties {

        /**
         * Адрес удалённого сервиса, например {@code tank-xxx0yyy123:9080}.
         */
        String restServiceEndpoint;
    }

    @Bean
    PerfectStoreEndpointApi perfectStoreEndpoint(X5HackApplication.HackAppProperties hackAppProperties) {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(hackAppProperties.getRestServiceEndpoint());

        OkHttpClient httpClient = apiClient.getHttpClient();
        httpClient.setConnectTimeout(5, TimeUnit.SECONDS);
        httpClient.setReadTimeout(10, TimeUnit.SECONDS);

        return new PerfectStoreEndpointApi(apiClient);
    }

    public static void main(String[] args) {
        var application = new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF)
                .sources(X5HackApplication.class)
                .web(WebApplicationType.NONE)
                .build();

        //noinspection MagicNumber
        application.setApplicationStartup(new BufferingApplicationStartup(10240));

        //noinspection resource
        application.run(args);
    }
}
