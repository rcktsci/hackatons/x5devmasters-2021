package com.gitlab.rcktsci.hackatons.x5.manager.prices;

import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.core.annotation.Order;

import java.util.Map;

@Order(10)
// @Service
@ConditionalOnMissingBean({
        ElasticPriceProvider.class
})
@RequiredArgsConstructor
@Slf4j
public class StaticPriceWithFinalSale implements PriceProvider, NamedFeature, SetupAtZero, EachTickExecutor {

    public static final double DESIRED_PROFIT = 1;
    public static final double FINAL_SALE = 0.0;

    private static final int CONSTANT_PRICE_PERIOD = 6 * Values.DAY + 12 * Values.HOUR;

    public static final Map<Integer, Double> PRODUCT_PROFITS = EntryStream.<Integer, Double>empty()
            /*
            .append(1, 0.2225)
            .append(2, 0.2125)
            .append(3, 0.2325)
            .append(4, 0.2400)
            .append(5, 0.2325)
            .append(6, 0.2325)
            .append(7, 0.2325)
            .append(8, 0.2125)
            .append(9, 0.2125)
            .append(10, 0.2325)
            .append(11, 0.2325)
            .append(12, 0.2125)
            .append(13, 0.2400)
            .append(14, 0.2125)
            .append(15, 0.2400)
            .append(1, 0.2200)
            .append(2, 0.2125)
            .append(3, 0.2325)
            .append(4, 0.2400)
            .append(5, 0.2325)
            .append(6, 0.2325)
            .append(7, 0.2325)
            .append(8, 0.2125)
            .append(9, 0.2125)
            .append(10, 0.2325)
            .append(11, 0.2325)
            .append(12, 0.2125)
            .append(13, 0.2400)
            .append(14, 0.2125)
            .append(15, 0.2400)
            .append(16, 0.2325)
            .append(17, 0.2400)
            .append(18, 0.2125)
            .append(19, 0.2400)
            .append(20, 0.2325)
            .append(21, 0.2125)
            .append(22, 0.2325)
            .append(23, 0.2275)
            .append(24, 0.2400)
            .append(25, 0.2400)
            .append(26, 0.2325)
            .append(27, 0.2325)
            .append(28, 0.2125)
            .append(29, 0.2275)
            .append(30, 0.2325)
            .append(31, 0.2325)
            .append(32, 0.2325)
            .append(33, 0.2325)
            .append(34, 0.2125)
            .append(35, 0.2325)
            .append(36, 0.2125)
            .append(37, 0.2325)
            .append(38, 0.2125)
            .append(39, 0.2400)
            .append(40, 0.2125)
            .append(41, 0.2400)
            .append(42, 0.2325)
            .append(43, 0.2325)
            .append(44, 0.2475)
            .append(45, 0.2125)
            .append(46, FINAL_SALE)
             */
            .toImmutableMap();

    @Override
    public void setup() {
        // log.info("Target: {}", 100 * TARGET_GAIN);
    }

    @Override
    public void tick() {
        // Обновляем цены каждые 10 минут.
        if (WorldContext.getCurrentTick() % 10 > 0) {
            return;
        }

        Map<Integer, Product> stockMap = WorldContext.getStockMap();

        EntryStream.of(stockMap)
                .mapValues(this::getSellPrice)
                .mapKeyValue(CmdFactory::price)
                .forEach(CommandsContext::addPrice);
    }

    @Override
    public double getSellPrice(Product product) {
        int currentTick = WorldContext.getCurrentTick();
        int totalTicks = WorldContext.getTickCount();

        int productId = product.getId();
        double baseProfit = PRODUCT_PROFITS.getOrDefault(productId, DESIRED_PROFIT);

        int now = currentTick - CONSTANT_PRICE_PERIOD;
        int total = totalTicks - CONSTANT_PRICE_PERIOD;
        double saleProgress = currentTick < CONSTANT_PRICE_PERIOD
                ? .0
                : now * 1.0 / total;

        double maximumSale = baseProfit - FINAL_SALE;
        double currentSale = saleProgress * maximumSale;
        double currentProfit = baseProfit - currentSale;
        double priceMultiplier = 1 + currentProfit;

        double stockPrice = product.getStockPrice();
        return stockPrice * priceMultiplier;
    }
}
