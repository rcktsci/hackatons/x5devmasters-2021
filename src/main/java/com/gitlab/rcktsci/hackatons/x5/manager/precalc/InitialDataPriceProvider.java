package com.gitlab.rcktsci.hackatons.x5.manager.precalc;

import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.manager.prices.PriceProvider;
import lombok.Value;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.NotImplementedException;

import java.util.List;

@Value
public class InitialDataPriceProvider implements PriceProvider {

    List<InitialData.SuperData> sellRateData;

    @Override
    public double getSellPrice(Product product, int rackCellRank) {
        return StreamEx.of(sellRateData)
                .filterBy(InitialData.SuperData::getProductId, product.getId())
                .filterBy(InitialData.SuperData::getRackCellRank, rackCellRank)
                .findFirst()
                .map(InitialData.SuperData::getSellPrice)
                .orElse(0.0);
    }

    @Override
    public double getSellPrice(Product product) {
        throw new NotImplementedException("Not used from static context.");
    }
}
