package com.gitlab.rcktsci.hackatons.x5.manager.monitors;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.CurrentWorldResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@Slf4j
public class WalletPrinter implements AtTheEndLogger {

    @Override
    public void print() {
        CurrentWorldResponse world = WorldContext.get();

        // Пора считать прибыль/убытки.
        double income = world.getIncome();
        double salaryCosts = world.getSalaryCosts();
        double stockCosts = world.getStockCosts();
        double result = income - salaryCosts - stockCosts;

        log.info("""
                        -------------------------------
                        Доход       {} ₽
                         - зарплата {} ₽
                         - закупки  {} ₽
                        -------------------------------
                              ИТОГО {} ₽
                        """,
                BigDecimal.valueOf(income).toPlainString(),
                BigDecimal.valueOf(salaryCosts).toPlainString(),
                BigDecimal.valueOf(stockCosts).toPlainString(),
                BigDecimal.valueOf(result).toPlainString());
    }
}
