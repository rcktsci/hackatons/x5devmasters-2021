package com.gitlab.rcktsci.hackatons.x5.manager.analysis.charge;

import com.gitlab.rcktsci.hackatons.x5.api.AtTheEndLogger;
import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.EachTickExecutor;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.api.stats.ListPerTickStatistic;
import com.gitlab.rcktsci.hackatons.x5.api.stats.ReduceMode;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import com.gitlab.rcktsci.hackatons.x5.manager.prices.StaticPriceWithFinalSale;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.springframework.core.annotation.Order;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

@SuppressWarnings("ConstantMathCall")
@Order(1)
// @Service
@RequiredArgsConstructor
@Slf4j
public class ChargeEvaluator implements NamedFeature, SetupAtZero, EachTickExecutor, AtTheEndLogger {

    /**
     * Нужно поставить 45 / 3 = 15 экспериментов :(
     * <br/>
     * Точнее, с <b>0-го по 14-й</b>.
     */
    private static final int EXPERIMENT_SERIES_ID = 1;

    private static final int INITIAL_BUYING = 100_000;
    private static final double START_AT = StaticPriceWithFinalSale.DESIRED_PROFIT;
    private static final double DELTA_STEP = 0.025;
    private static final double STOP_AT = StaticPriceWithFinalSale.FINAL_SALE;
    private static final int PERIODS = (int) Math.floor(Math.abs(START_AT - STOP_AT) / DELTA_STEP);
    private static final int PERIOD_LENGTH = 7 * Values.DAY / PERIODS;

    @SuppressWarnings("MagicNumber")
    private double prevMultiplier = -1000000.0;

    private final Map<Integer, PriceExperiment> experimentsByRack = new HashMap<>();
    private final Collection<PriceExperiment> experimentHistory = new ArrayList<>();
    private final ListPerTickStatistic charges = new ListPerTickStatistic("charge-variants", ReduceMode.LAST, 1);

    @Override
    public void setup() {
        setup(ChargeEvaluator::setupTick0);
        updatePrices();
        log.info("Макс. {} %, мин. {} %, шаг {} %, шагов {}, по {} минут.",
                StaticPriceWithFinalSale.DESIRED_PROFIT,
                StaticPriceWithFinalSale.FINAL_SALE,
                DELTA_STEP,
                PERIODS,
                PERIOD_LENGTH);
    }

    @Override
    public void tick() {
        if (WorldContext.getCurrentTick() == 1) {
            setup(ChargeEvaluator::setupTick1);
            restartExperiments();
            return;
        }

        refill();
        updatePrices();
    }

    private static void setup(BiConsumer<Integer, RackCell> action) {
        List<RackCell> sortedRacks = WorldContext.getSortedRacks();
        AtomicInteger rackId = new AtomicInteger();

        IntStreamEx.range(0, 45)
                .map(idx -> 1 + (idx + 3 * EXPERIMENT_SERIES_ID) % 45)
                .boxed()
                .limit(sortedRacks.size())
                .mapToEntry(ignored -> rackId.getAndIncrement())
                .mapValues(sortedRacks::get)
                .forKeyValue(action);
    }

    private static void setupTick0(int productId, RackCell rackCell) {
        var buy = CmdFactory.buy(productId, INITIAL_BUYING);
        CommandsContext.addBuy(buy);
    }

    private static void setupTick1(int productId, RackCell rackCell) {
        var cmd = CmdFactory.toRackCell(rackCell.getId(), productId, rackCell.getCapacity());
        CommandsContext.addOnRack(cmd);
    }

    private void updatePrices() {
        int periodId = WorldContext.getCurrentTick() / PERIOD_LENGTH;
        double multiplier = StaticPriceWithFinalSale.DESIRED_PROFIT - periodId * DELTA_STEP;

        //noinspection MagicNumber
        if (Math.abs(prevMultiplier - multiplier) < 0.00001) {
            return;
        }

        // log.info("{}) Наценка {} %", WorldContext.getCurrentTick(), 100 * multiplier);

        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        EntryStream.of(stockMap)
                .values()
                .mapToEntry(p -> p.getStockPrice() * (1 + multiplier))
                .mapKeys(Product::getId)
                .mapKeyValue(CmdFactory::price)
                .forEach(CommandsContext::addPrice);

        restartExperiments();
        prevMultiplier = multiplier;
    }

    private void restartExperiments() {
        int currentTick = WorldContext.getCurrentTick();
        finishExperimentSeries();
        experimentsByRack.clear();

        Map<Integer, Integer> volumes = WorldContext.sumProductsOnRackCellsAndInStock();
        rackCellProductStream()
                .mapKeys(RackCell::getId)
                .mapValues(Product::getId)
                .mapValues(volumes::get)
                .mapValues(sum -> new PriceExperiment(sum, currentTick))
                .forKeyValue(experimentsByRack::put);
    }

    private static void refill() {
        // Пополнить полки с товарами.
        rackCellProductStream()
                .mapKeyValue(CmdFactory::toRackCell)
                .forEach(CommandsContext::addOnRack);
    }

    private static EntryStream<RackCell, Product> rackCellProductStream() {
        Map<Integer, RackCell> rackCellMap = WorldContext.getRackCellMap();
        Map<Integer, Product> stockMap = WorldContext.getStockMap();

        return EntryStream.of(rackCellMap)
                .values()
                .mapToEntry(RackCell::getProductId)
                .nonNullValues()
                .mapValues(stockMap::get)
                .nonNullValues();
    }

    private void finishExperimentSeries() {
        int currentTick = WorldContext.getCurrentTick();

        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        Map<Integer, RackCell> rackCellMap = WorldContext.getRackCellMap();
        Map<Integer, Integer> volumes = WorldContext.sumProductsOnRackCellsAndInStock();

        List<Integer> result = EntryStream.of(experimentsByRack)
                .mapKeys(rackCellMap::get)
                .mapToValue((rc, e) -> {
                    int productId = rc.getProductId();
                    int sum = volumes.getOrDefault(productId, 0);
                    e.setFinishAmount(sum);
                    e.setFinishTick(currentTick);
                    double sellRate = e.calculateSellRate();
                    int weekSellAmount = (int) (sellRate * Values.WEEK);

                    Product product = stockMap.get(productId);
                    double stockPrice = product.getStockPrice();
                    double deltaPrice = stockPrice * prevMultiplier;
                    double weeklyProfit = weekSellAmount * deltaPrice;

                    e.setProductId(productId);
                    e.setRackCellRank(rc.getVisibility());
                    e.setPriceMultiplier(prevMultiplier);
                    e.setWeeklyProfit(weeklyProfit);

                    return weekSellAmount;
                })
                .values()
                .toList();

        experimentHistory.addAll(experimentsByRack.values());
        charges.put(currentTick, result);
    }

    @Override
    public void print() {
        charges.print();
        var data = StreamEx.of(experimentHistory)
                .sortedBy(PriceExperiment::getProductId)
                .mapToEntry(PriceExperiment::getProductId, e -> e)
                .collapseKeys()
                .mapValues(ChargeEvaluator::filterExperiments)
                .mapKeyValue(ChargeEvaluator::pack)
                .toList();
        String output = LogUtils.encodeGzipString(LogUtils.toJson(data));
        log.info("Серия экспериментов # {}:\n{}", EXPERIMENT_SERIES_ID, output);
    }

    private static PriceExperimentData pack(int productId, Collection<PriceExperiment> experiments) {
        return PriceExperimentData.builder()
                .productId(productId)
                .rackCellRank(StreamEx.of(experiments)
                        .mapToEntry(
                                PriceExperiment::getRackCellRank,
                                e -> new PriceExperimentData.Result(
                                        e.getPriceMultiplier(),
                                        e.getWeeklyProfit()))
                        .toMap())
                .build();
    }

    private static List<PriceExperiment> filterExperiments(Collection<PriceExperiment> experiments) {
        return IntStreamEx.range(1, 6)
                .mapToObj(rackCellRank -> StreamEx.of(experiments)
                        .filter(pe -> pe.getRackCellRank() == rackCellRank)
                        .maxBy(PriceExperiment::getWeeklyProfit)
                        .orElse(null))
                .nonNull()
                .toList();
    }
}
