package com.gitlab.rcktsci.hackatons.x5.manager.analysis;

import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement.SellRateResult;
import com.gitlab.rcktsci.hackatons.x5.manager.prices.PriceProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
@Slf4j
public class BruteForceService {

    private final PriceProvider priceProvider;

    @SuppressWarnings("BooleanParameter")
    public List<BruteForceResult> bruteForce(
            Collection<SellRateResult> forecasts,
            boolean excludeAbsentProducts
    ) {
        return bruteForce(forecasts, excludeAbsentProducts, priceProvider);
    }

    @SuppressWarnings("BooleanParameter")
    public static List<BruteForceResult> bruteForce(
            Collection<SellRateResult> forecasts,
            boolean excludeAbsentProducts,
            PriceProvider priceProvider
    ) {
        List<BruteForceResult> result = new ArrayList<>();

        Collection<Integer> excludedProducts = new HashSet<>();
        Map<Integer, Set<Integer>> excludedProductsByRank = new HashMap<>();
        excludedProductsByRank.put(1, new HashSet<>());
        excludedProductsByRank.put(2, new HashSet<>());
        excludedProductsByRank.put(3, new HashSet<>());
        excludedProductsByRank.put(4, new HashSet<>());
        excludedProductsByRank.put(5, new HashSet<>());
        if (excludeAbsentProducts) {
            Map<Integer, Integer> volumesByProductId = WorldContext.sumProductsOnRackCellsAndInStock();
            EntryStream.of(volumesByProductId)
                    .filterValues(v -> v == 0)
                    .keys()
                    .forEach(e -> {
                        excludedProducts.add(e);
                        excludedProductsByRank.values().forEach(l -> l.add(e));
                    });
        }

        List<RackCell> sortedRacks = WorldContext.getSortedRacks();
        for (RackCell rackCell : sortedRacks) {
            int rank = rackCell.getVisibility();
            Set<Integer> excludedProductOfThisRank = excludedProductsByRank.get(rank);

            StreamEx.of(forecasts)
                    .filterBy(SellRateResult::getRackCellRank, rank)
                    .filter(srr -> !excludedProducts.contains(srr.getProductId()))
                    // .filter(srr -> !excludedProductOfThisRank.contains(srr.getProductId()))
                    // Сколько шт. планируем продать?
                    .map(srr -> toBruteForceResult(rackCell, srr))
                    // Сколько заработаем на продаже?
                    .maxBy(bfr -> calculateIncome(bfr, priceProvider, rank))
                    .ifPresent(bfr -> {
                        int productId = bfr.productId();
                        excludedProducts.add(productId);
                        excludedProductOfThisRank.add(productId);
                        result.add(bfr);
                    });
        }

        return result;
    }

    private static BruteForceResult toBruteForceResult(RackCell rackCell, SellRateResult sellRateResult) {
        int rackCellId = rackCell.getId();
        int productId = sellRateResult.getProductId();
        int ticksUntilEnd = WorldContext.getTicksUntilEnd();

        // Сколько шт. планируем продать?
        int amount = (int) (sellRateResult.getSellRate() * ticksUntilEnd);
        return new BruteForceResult(rackCellId, productId, amount, new AtomicReference<>());
    }

    private static double calculateIncome(
            BruteForceResult bruteForceResult,
            PriceProvider priceProvider,
            int rackCellRank
    ) {
        int productId = bruteForceResult.productId();
        Map<Integer, Product> stockMap = WorldContext.getStockMap();
        Product product = stockMap.get(productId);

        double stockPrice = product.getStockPrice();
        double sellPrice = priceProvider.getSellPrice(product, rackCellRank);
        double profit = sellPrice - stockPrice;

        // Сколько заработаем на продаже?
        double income = bruteForceResult.amountToSell() * profit;
        bruteForceResult.expectedIncome().set(income);
        return income;
    }

    @SuppressWarnings("BooleanParameter")
    public static List<BruteForceResult> bruteForce2(
            Collection<SellRateResult> forecasts,
            boolean excludeAbsentProducts,
            PriceProvider priceProvider
    ) {
        List<RackCell> sortedRacks = WorldContext.getSortedRacks();
        Map<Integer, RackCell> rackCellMap = WorldContext.getRackCellMap();

        double maxTotalIncome = 0;
        List<BruteForceResult> bestResult = List.of();

        Iterator<int[]> iterator = CombinatoricsUtils.combinationsIterator(45, 15);
        while (iterator.hasNext()) {
            int[] combination = iterator.next();
            long distinct = IntStreamEx.of(combination)
                    .distinct()
                    .count();
            if (combination.length != distinct) {
                continue;
            }

            double totalIncome = 0.0;
            List<BruteForceResult> result = new ArrayList<>(15);
            for (int rc = 1; rc <= combination.length; rc += 1) {
                RackCell rackCell = rackCellMap.get(rc);
                int rank = rackCell.getVisibility();
                int productId = 1 + combination[rc - 1];
                BruteForceResult item = StreamEx.of(forecasts)
                        .filterBy(SellRateResult::getRackCellRank, rank)
                        .filterBy(SellRateResult::getProductId, productId)
                        .map(srr -> toBruteForceResult(rackCell, srr))
                        .maxBy(bfr -> calculateIncome(bfr, priceProvider, rank))
                        .orElse(null);
                if (item == null) {
                    continue;
                }

                result.add(item);
                totalIncome += item.expectedIncome().get();
            }

            if (maxTotalIncome < totalIncome) {
                maxTotalIncome = totalIncome;
                bestResult = result;
            }
        }

        return bestResult;
    }

    public static record BruteForceResult(
            int rackCellId,
            int productId,
            int amountToSell,
            AtomicReference<Double> expectedIncome
    ) {

    }
}
