package com.gitlab.rcktsci.hackatons.x5.manager.supplier;

import com.gitlab.rcktsci.hackatons.x5.api.CommandsContext;
import com.gitlab.rcktsci.hackatons.x5.api.NamedFeature;
import com.gitlab.rcktsci.hackatons.x5.api.SetupAtZero;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.Product;
import com.gitlab.rcktsci.hackatons.x5.codegen.model.RackCell;
import com.gitlab.rcktsci.hackatons.x5.manager.precalc.InitialData;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.CmdFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.EntryStream;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.Map;

@ConditionalOnMissingBean(GenericSupplier.class)
@Order(10)
@Service
@RequiredArgsConstructor
@Slf4j
public class ExperimentSupplier implements NamedFeature, ProductsSupplier, SetupAtZero {

    private static final int INITIAL_AMOUNT = 20_000;

    private final InitialData initialData;

    @Override
    public void setup() {
        Map<RackCell, Product> table = initialData.getExperimentTable();
        EntryStream.of(table)
                .values()
                .map(Product::getId)
                .mapToEntry(v -> INITIAL_AMOUNT)
                .mapKeyValue(CmdFactory::buy)
                .forEach(CommandsContext::addBuy);
    }

    @Override
    public void resupply(Map<Integer, Integer> currentNeeds) {
        // ¯\_(ツ)_/¯
    }
}
