package com.gitlab.rcktsci.hackatons.x5.manager.prices;

import com.gitlab.rcktsci.hackatons.x5.manager.analysis.SimpleMeasure;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class ElasticityMeasure extends SimpleMeasure {

    private double sellPrice;
    private int rackCellId;
    private ElasticityMeasure prev;
    private double requestElasticityByPrice;

    public ElasticityMeasure(int initialAmount, int initialTick) {
        super(initialAmount, initialTick);
    }
}
