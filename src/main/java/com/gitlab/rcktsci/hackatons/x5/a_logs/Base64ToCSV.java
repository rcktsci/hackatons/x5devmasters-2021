package com.gitlab.rcktsci.hackatons.x5.a_logs;

import com.gitlab.rcktsci.hackatons.x5.manager.Values;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils;
import com.gitlab.rcktsci.hackatons.x5.manager.utilities.LogUtils.LogDecodeResult;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

@UtilityClass
@Slf4j
public class Base64ToCSV {

    private static final Collection<Function<String, LogDecodeResult>> DECODERS = List.of(
            base64 -> LogDecodeResult.fromIntegerTable(LogUtils.decodeIntegerTable(base64)),
            LogUtils::decodeBZip2Csv,
            LogUtils::decodeGzipCsv);

    @SneakyThrows
    public static void main(String[] args) {
        String base64 = LogUtils.readClipboardBase64();
        var decodedResult = decodeData(base64);

        String fileBody = toExcelCsv(decodedResult.payload());

        Path pathToFile = findFreeFileName(decodedResult.name());
        Files.writeString(pathToFile, fileBody);

        log.info("Statistics file '{}' has been saved.", pathToFile);
    }

    private static LogDecodeResult decodeData(String base64) {
        for (var decoder : DECODERS) {
            try {
                return decoder.apply(base64);
            } catch (Exception ignored) {
            }
        }
        throw new IllegalStateException("Скопируй в буфер корректные данные!");
    }

    private static Path findFreeFileName(String statsName) {
        return IntStreamEx.iterate(0, i -> i + 1)
                .mapToObj(i -> "_stats__%s__%d.csv".formatted(statsName, i))
                .map(File::new)
                .filter(f -> !f.isFile())
                .map(File::toPath)
                .findFirst()
                .orElseThrow();
    }

    private static String toExcelCsv(String data) {
        return StreamEx.of(data.split("\n"))
                .map(l -> l.split(";", 2))
                .mapToEntry(l -> l[0], l -> l[1])
                .mapKeys(Integer::parseInt)
                .mapKeys(time -> "%d %s".formatted(
                        1 + time / Values.DAY,
                        LocalTime.ofSecondOfDay(time % Values.DAY * 60)))
                .join("; ")
                // Excel на русской локали ¯\_(ツ)_/¯.
                .map(line -> line.replace('.', ','))
                .joining("\n");
    }
}
