package com.gitlab.rcktsci.hackatons.x5.manager.utilities;

import com.gitlab.rcktsci.hackatons.x5.api.WorldContext;

import java.time.LocalTime;

/**
 * Периоды времени дня, различающиеся по частоте прихода клиентов.
 */
public enum HourOfDay {

    NIGHT(
            LocalTime.of(0, 0),
            LocalTime.of(7, 59)),

    MORNING_PEAK(
            LocalTime.of(8, 0),
            LocalTime.of(8, 59)),

    DAY(
            LocalTime.of(9, 0),
            LocalTime.of(18, 59)),

    EVENING_PEAK(
            LocalTime.of(19, 0),
            LocalTime.of(21, 59)),

    NIGHT_END_OF_DAY(
            LocalTime.of(22, 0),
            LocalTime.of(23, 59)),
    ;

    private final int start;
    private final int end;

    HourOfDay(LocalTime start, LocalTime end) {
        this.start = start.getHour() * 60 + start.getMinute();
        this.end = end.getHour() * 60 + end.getMinute();
    }

    public static HourOfDay current() {
        int minuteOfDay = WorldContext.getTickOfDay();
        for (HourOfDay value : values()) {
            if (value.start <= minuteOfDay && minuteOfDay <= value.end) {
                return value;
            }
        }

        // Никак нет.
        throw new IllegalStateException("Never");
    }
}
