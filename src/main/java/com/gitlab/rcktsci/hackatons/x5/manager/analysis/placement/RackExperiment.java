package com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement;

import lombok.Data;

@Data
public class RackExperiment {

    private final int rackCellRank;
    private final int productGroupId;

    private boolean isStarted;
    private int startedAt;

    private boolean isFinished;
    private int finishedAt;
}
