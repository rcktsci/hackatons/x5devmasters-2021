package com.gitlab.rcktsci.hackatons.x5.manager.analysis.placement;

import java.util.List;

public interface SellRateAnalyzer {

    boolean isInAnalyzingPeriod();

    List<SellRateResult> calculateFinalRates();
}
