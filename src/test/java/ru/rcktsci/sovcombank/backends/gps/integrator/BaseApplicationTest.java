package ru.rcktsci.sovcombank.backends.gps.integrator;

import com.gitlab.rcktsci.hackatons.x5.X5HackApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(
        classes = X5HackApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@Slf4j
public class BaseApplicationTest {

    @DisplayName(value = "I'm okay.")
    @Test
    public void contextIsCorrect() {
        log.info("I'm fine, thank you.");
    }
}
